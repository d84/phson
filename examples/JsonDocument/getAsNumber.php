<?php
require __DIR__ . '/../../vendor/autoload.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "price1": 9.99,
  "price2": 15,
  "price3": "5",
  "price4": "1.55",
  "string": "Hello!"
}
JSON;

$element = null;
try {
    $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

$price1 = $element->asNumber('price1');
$price2 = $element->asNumber('price2');
$price3 = $element->asNumber('price3');
$price4 = $element->asNumber('price4');
$string = $element->asNumber('string');

echo "Price1: $price1 [" . gettype($price1) . "]\n";
echo "Price2: $price2 [" . gettype($price2) . "]\n";
echo "Price3: $price3 [" . gettype($price3) . "]\n";
echo "Price4: $price4 [" . gettype($price4) . "]\n";
echo "String: $string [" . gettype($string) . "]\n";
