<?php
require __DIR__ . '/../../vendor/autoload.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "user": {
    "login": "user",
    "profile": {
      "logo": "http://localhost/users/user/logo.png",
      "uid": 334239091
    }
  }
}
JSON;

$element = null;
try {
    $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

// Chain of the get methods
$logo_pic = $element->get('user')->get('profile')->asString('logo');

// Find element by path
$uid = $element->find('user/profile/uid')->asInteger();

echo "LogoPic: $logo_pic \n";
echo "UID: $uid \n";
