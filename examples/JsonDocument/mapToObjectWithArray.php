<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../Mocks/TokenMessage.php';
require __DIR__ . '/../Mocks/Author.php';
require __DIR__ . '/../Mocks/Token.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "id": 1302302,
  "text": "Hello!",
  "tokens": [
    {
      "key": "$2$56.3323FGd23dsd3",
      "author": {
        "name": "jill56"
      }
    },
    {
      "key": "$5$6.gh#4321dff4",
      "author": {
        "name": "mick.swd"
      }
    }
  ]
}
JSON;

$msg = null;
try {
    $msg = Phson::fromJson(
        $json,
        TokenMessage::class,
        [
          'tokens' => Token::class,
          'author' => Author::class
        ]
    );
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

echo "MsgId: " . $msg->id . ", MsgText: " . $msg->text . "\n";
foreach ($msg->tokens as $token) {
    echo "Key: [" . $token->key . "] <" . $token->author->name . ">\n";
}
