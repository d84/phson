<?php
require __DIR__ . '/../../vendor/autoload.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "count": 13443
}
JSON;

$element = null;
try {
    $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

$count = $element->asInteger('count');

echo "Count: $count [" . gettype($count) . "]";
