<?php
require __DIR__ . '/../../vendor/autoload.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "sysdate": "01.01.2018 00:00:00",
  "date_1": 1,
  "date_2": true
}
JSON;

$element = null;
try {
    $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

// instance of the Carbon class
$sysdate = $element->asDate('sysdate');
$date_1 = $element->asDate('date_1');
$date_2 = $element->asDate('date_2');

echo "Sysdate: $sysdate [" . gettype($sysdate) . '|' . get_class($sysdate) . "]\n";
echo "Date_1: $date_1 [" . gettype($date_1) . '|' . get_class($date_1) . "]\n";

// asDate returns FALSE in case when it was not able to get a property value as Date
if ($date_2 === false) {
    echo "Unable to get 'date_2' as Date";
}
