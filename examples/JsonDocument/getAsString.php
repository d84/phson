<?php
require __DIR__ . '/../../vendor/autoload.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "dayOfWeek": "monday"
}
JSON;

$element = null;
try {
    $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

$dayOfWeek = $element->asString('dayOfWeek');

echo "Day of week: $dayOfWeek [" . gettype($dayOfWeek) . "]";
