<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../Mocks/SimpleMessage.php';

use d84\Phson\Facade\Phson;

// In case when the JSON document field name is different than the PHP object property name, the PHP object
// must implement JsonSerializable and JsonDeserializable interfaces.
// Please see Mocks/SimpleMessage

$json = <<<JSON
{
  "MESSAGE_ID": 1302302,
  "MESSAGE_TEXT": "Hello!"
}
JSON;

$msg = null;
try {
    $msg = Phson::fromJson($json, SimpleMessage::class);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

echo $msg . "\n";

$msg->text = $msg->text . " <extra-text>";

echo Phson::toJson($msg, ['prettyprint' => true]);
