<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../Mocks/UserMessage.php';
require __DIR__ . '/../Mocks/Address.php';
require __DIR__ . '/../Mocks/User.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "id": 1302302,
  "text": "Hello!",
  "user": {
    "username": "User33",
    "address": {
      "country": "Russia",
      "city": "Vladivostok",
      "zip": 690091
    }
  }
}
JSON;

$msg = null;
try {
    $msg = Phson::fromJson(
        $json,
        UserMessage::class,
        [
          'user' => User::class,
          'address' => Address::class
        ]
    );
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

echo "MsgId: " . $msg->id . "\n";
echo "MsgText: " . $msg->text . "\n";
echo "UserName: " . $msg->user->username . "\n";
echo "Address: " . $msg->user->address->country . ", "
                 . $msg->user->address->city . ", "
                 . $msg->user->address->zip . "\n";
