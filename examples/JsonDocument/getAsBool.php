<?php
require __DIR__ . '/../../vendor/autoload.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "isError": false,
  "isSuccess": true,
  "intTrue": 1,
  "intFals": 0,
  "number": 99,
  "stringTrue": "TRUE",
  "stringFalse": "FALSE",
  "string": "Hello!"
}
JSON;

$element = null;
try {
    $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

$isError = $element->asBoolean('isError');
$isSuccess = $element->asBoolean('isSuccess');
$intTrue = $element->asBoolean('intTrue');
$intFals = $element->asBoolean('intFals');
$number = $element->asBoolean('number');
$stringTrue = $element->asBoolean('stringTrue');
$stringFalse = $element->asBoolean('stringFalse');
$string = $element->asBoolean('string');

echo "isError: " . (int)$isError ." [" . gettype($isError) . "]\n";
echo "isSuccess: " . (int)$isSuccess ." [" . gettype($isSuccess) . "]\n";
echo "intTrue: " . (int)$intTrue ." [" . gettype($intTrue) . "]\n";
echo "intFals: " . (int)$intFals ." [" . gettype($intFals) . "]\n";
echo "number: " . (int)$number ." [" . gettype($number) . "]\n";
echo "stringTrue: " . (int)$stringTrue ." [" . gettype($stringTrue) . "]\n";
echo "stringFalse: " . (int)$stringFalse ." [" . gettype($stringFalse) . "]\n";
echo "string: " . (int)$string ." [" . gettype($string) . "]\n";
