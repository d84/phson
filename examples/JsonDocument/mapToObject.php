<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../Mocks/SimpleMessage.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "id": 1302302,
  "text": "Hello!"
}
JSON;

$msg = null;
try {
    $msg = Phson::fromJson($json, SimpleMessage::class);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

echo "MsgId: " . $msg->id . ", MsgText: " . $msg->text;
