<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../Mocks/SimpleMessage.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
[
  {
    "id": 1,
    "text": "A house is not a home"
  },
  {
    "id": 2,
    "text": "A golden key can open any door"
  },
  {
    "id": 3,
    "text": "A little knowledge is a dangerous thing"
  }
]
JSON;

$msgs = null;
try {
    $msgs = Phson::fromJson($json, SimpleMessage::class);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

foreach ($msgs as $msg) {
    echo $msg;
}
