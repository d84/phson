<?php
require __DIR__ . '/../../vendor/autoload.php';

use d84\Phson\Facade\Phson;

$json = <<<JSON
{
  "price": 9.99
}
JSON;

$element = null;
try {
    $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
    die("JSON parsing failed due to: " . $rte->getMessage());
}

$price = $element->asFloat('price');

echo "Price: $price [" . gettype($price) . "]";
