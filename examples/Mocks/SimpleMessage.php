<?php

use d84\Phson\Document\JsonDeserializable;

/**
 * JsonSerializable - php standard interface
 * JsonDeserializable - pson interface
 */
class SimpleMessage implements JsonSerializable, JsonDeserializable
{
    public $id;
    public $text;

    public function jsonSerialize()
    {
        return [
          'MESSAGE_ID' => $this->id,
          'MESSAGE_TEXT' => $this->text
        ];
    }

    public function jsonDeserialize()
    {
        return [
          'MESSAGE_ID' => 'id',
          'MESSAGE_TEXT' => 'text',
        ];
    }

    public function __toString()
    {
        return "Msg:\n"
        . "ID: " . $this->id . "\n"
        . "Text: " . $this->text . "\n\n";
    }
}
