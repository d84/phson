<?php

class TokenMessage
{
    public $id;
    public $text;
    public $tokens;

    public function __toString()
    {
        return "Msg:\n"
        . "ID: " . $this->id . "\n"
        . "Text: " . $this->text . "\n\n";
    }
}
