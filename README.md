# phson
----------------------------------------------
A convenient approach to work with the JSON data.
The phson library covers following issues:

* JSON -> php object
* php object -> JSON
* validate JSON by given JSON schema

The 'php object' can be either the instance of JsonElementAbstract class or your domain object (for more details see the
'tests\jsondoc\JsonDocumentTest.php' file).

JsonDocument datatypes:

* Primitive: int, float, number, string, bool, date(string/timestamp).
* Null
* Object
* Array

## Getting Started
----------------------------------------------

##### Install

*Clone repository*
```
git clone https://d84@bitbucket.org/d84/phson.git
```

*Composer*

Add to the composer.json
```
"repositories": [
  {
    "type": "git",
    "url": "https://bitbucket.org/d84/phson"
  }
]

```

require:
```
"d84/phson": "0.1.0"
```


##### Run the tests
* ant: `$ ant phpunit`
* composer: `$ composer test`
* composer (testdox): `$ composer testc`

## How to use
----------------------------------------------

### Work with the objects

#### Properties

###### 1. *Get a property value as String*
```php
<?
use d84\Phson\Facade\Phson;

$json = '
{
  "dayOfWeek": "monday"
}';

$element = null;
try {
  $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$dayOfWeek = $element->asString('dayOfWeek');
```

###### 2. *Get a property value as Int*
```php
<?php
use d84\Phson\Facade\Phson;

$json = '
{
  "count": 13443
}';

$element = null;
try {
  $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$count = $element->asInteger('count');
```

###### 3. *Get a property value as Float*
```php
<?php
use d84\Phson\Facade\Phson;

$json = '
{
  "price": 9.99
}';

$element = null;
try {
  $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$price = $element->asFloat('price');
```

###### 4. *Get a property value as Number*
```php
<?php
use d84\Phson\Facade\Phson;

$json = '
{
  "price1": 9.99,
  "price2": 15,
  "price3": "5",
  "price4": "1.55"
}';

$element = null;
try {
  $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$price1 = $element->asNumber('price1');
$price2 = $element->asNumber('price2');
$price3 = $element->asNumber('price3');
$price4 = $element->asNumber('price4');
```

###### 5. *Get a property value as Boolean*
```php
<?php
use d84\Phson\Facade\Phson;

$json = '
{
  "isError": false
}';

$element = null;
try {
  $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$isError = $element->asBoolean('isError');
```

###### 6. *Get a property value as Date*
```php
<?php
use d84\Phson\Facade\Phson;

$json = '
{
  "sysdate": "01.01.2018 00:00:00"
}';

$element = null;
try {
  $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

// instance of the Carbon class
$sysdate = $element->asDate('sysdate');
```

###### 7. *Get a property value as inner object*
```php
<?php
use d84\Phson\Facade\Phson;

$json = '
{
  "user": {
    "login": "user",
    "profile": {
      "logo": "http://localhost/users/user/logo.png"
    }
  }
}';

$element = null;
try {
  $element = Phson::fromJson($json);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$logo_pic = $element->get('user')->get('profile')->asString('logo');
```

#### Map the JSON object to the PHP object

###### *Map an object to a particular class*
```php
<?php
use d84\Phson\Facade\Phson;

class Message
{
  public $id;
  public $text;
}

$json = '
{
  "id": 1302302,
  "text": "Hello!"
}';

$msg = null;
try {
  $msg = Phson::fromJson($json, Message::class);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$msg_id   = $msg->id;
$msg_text = $msg->text;
```

###### *Map nested object*
```php
<?php
use d84\Phson\Facade\Phson;

class User
{
  public $login;
}

class Message
{
  public $id;
  public $text;
  public $user;
}

$json = '
{
  "id": 1302302,
  "text": "Hello!",
  "user": {
    "login": "User1"
  }
}';

$msg = null;
try {
  $msg = Phson::fromJson($json, Message::class, ['user' => User::class]);
} catch (\RuntimeException $rte) {
  die("JSON parsing failed due to: " . $rte->getMessage());
}

$msg_id     = $msg->id;
$msg_text   = $msg->text;
$user_login = $msg->user->login;
```

## Development
----------------------------------------------

### Prerequisites
In order to obtain artefacts such as phpstan log, phpunit log etc. you should get install several tools:

* php 7 (dev machine)
* composer (https://getcomposer.org/)
* java (we use java version "1.8.0_151")
* ant (http://ant.apache.org/) (we use Apache Ant(TM) version 1.9.9)

### Build the artefacts
```
$ ant build
```
That command will perform several steps:

* composer-install: performs command: `$ composer install`
* clean: deletes 'build' directory if it exist
* prepare: creates 'build' directory with sub-directories for the artefacts
* lint: runs `$ php -l src/*`
* phpcs: checks code style (we use PSR2).
https://packagist.org/packages/squizlabs/php_codesniffer
* phpmd: detects mess in code.
https://packagist.org/packages/phpmd/phpmd
* phpstan: checks Static Analysis Tool.
https://packagist.org/packages/phpstan/phpstan
* phpunit: runs unit tests.
https://packagist.org/packages/phpunit/phpunit
