<?php
use d84\Phson\Document\Element\JsonElementAbstract;
use PHPUnit\Framework\TestCase;
use d84\Phson\Facade\Phson;

class MockUser
{
    public $username;
}

class MockMessage
{
    public $text;
    public $user;
}

class UseCaseTest extends TestCase
{
    /**
     * @test
     */
    public function validateWithMapNestedObject()
    {
        $json = '{
          "text": "Bla-Bla",
          "user": {
            "username": "User-3310"
          }
        }';

        $schema = '{
          "type": "object",
          "properties": {
            "text": {"type": "string"},
            "user": {
              "type": "object",
              "properties": {
                "username": {"type": "string"}
              }
            }
          }
        }';

        $msg = Phson::validate($json, $schema, MockMessage::class, ['user' => MockUser::class]);

        $this->assertEquals('User-3310', $msg->user->username);
    }

    /**
     * @test
     */
    public function workWithTheJsonDocument()
    {
        $json = '
        {
          "model":   "Sportage",
          "vin":     12344321233,
          "engine":  "v4",
          "producer": {
            "id":      25000231,
            "name":    "KIA",
            "address": {
              "state": "Primorsky-Krai",
              "city":  "Vladivostok",
              "zip":   690091
            }
          },
          "creation_date_ts": 946684800,
          "creation_date": "01.01.2000 00:00:00"
        }';

        $element = Phson::fromJson($json);

        // Get the 'model' property as string
        $model = $element->asString('model');

        // Get the 'vin' property as Int
        $vin = $element->asInteger('vin');

        // Get the nested property
        $prodname = $element->get('producer')->asString('name');

        // Get the nested property by path
        $zip = $element->find('producer/address/zip')->asNumber();

        // --------------------------------------------------------------
        // Date-time
        // --------------------------------------------------------------
        // timestamp => d.m.Y H:i:s (Carbon)
        $creation_date_from_ts = $element->asDate('creation_date_ts');
        // string datetime => d.m.Y H:i:s (Carbon)
        $creation_date_from_date = $element->asDate('creation_date');

        $day   = $creation_date_from_ts->day;
        $month = $creation_date_from_date->month;
        $year  = $creation_date_from_ts->year;

        if ($day < 10) {
            $day = '0' . $day;
        }
        if ($month < 10) {
            $month = '0' . $month;
        }

        // TEST

        $this->expectOutputString(
          "Model: Sportage, VIN: 12344321233, Producer: KIA, ZIP: 690091, CreationDate: 01.01.2000"
        );

        echo "Model: $model, VIN: $vin, Producer: $prodname, ZIP: $zip, CreationDate: $day.$month.$year";
    }

    /**
     * @test
     */
    public function validateJsonDocument()
    {
        $json = '{
          "id": 554433212331043,
          "message": "This is the test message!",
          "datetime": "10.01.2018 10:48:10"
        }';

        $schema = '{
          "type": "object",
          "properties": {
            "id": {"type": "number"},
            "message": {"type": "string", "maxLength": 255},
            "datetime": {"type": "string", "format": "date-time", "dt-format": "d.m.Y H:i:s"}
          }
        }';
        $msg = false;
        try {
            $msg = Phson::validate($json, $schema);
        } catch (\RuntimeException $rte) {
            // Exception handling
        }
        $this->assertInstanceOf(JsonElementAbstract::class, $msg);
    }
}
