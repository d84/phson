<?php
use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Facade\Phson;
use PHPUnit\Framework\TestCase;

class FacadeTest extends TestCase
{
    /**
     * @test
     */
    public function fromJson()
    {
        $el = Phson::fromJson('{"name": "Pavel"}');
        $this->assertEquals('Pavel', $el['name']->asString());
    }

    /**
     * @test
     */
    public function validate()
    {
        $this->assertInstanceOf(
          JsonElementAbstract::class,
          Phson::validate(
            '{"name": "Pavel"}',
            '{"type": "object", "properties": {"name": {"type": "string"}}}'
          )
        );
    }

    /**
     * @test
     */
    public function toJson()
    {
        $obj = Phson::fromArray([
          'id' => 123,
          'login' => 'sponge.bob',
          'password' => 'qwerty'
        ]);
        $json = Phson::toJson($obj);
        $this->assertEquals('{"id":123,"login":"sponge.bob","password":"qwerty"}', $json);
    }
}
