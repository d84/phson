<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Schema\Validator\Validator;

class SchemaNumberTest extends TestCase
{
    /**
     * @test
     */
    public function number()
    {
        $schema = '
        {
          "type": "number"
        }';
        $data = '123';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedElement()
    {
        $schema = '
        {
          "type": "number"
        }';
        $data = '[123]';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedPrimitiveType()
    {
        $schema = '
        {
          "type": "number"
        }';
        $data = '"123"';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkMaximumFault()
    {
        $schema = '
        {
          "type": "number",
          "maximum": 5
        }';
        $data = '123';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkExclusiveMaximumFault()
    {
        $schema = '
        {
          "type": "number",
          "exclusiveMaximum": true,
          "maximum": 5
        }';
        $data = '123';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkMinimumFault()
    {
        $schema = '
        {
          "type": "number",
          "minimum": 5
        }';
        $data = '5';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkExclusiveMinimumFault()
    {
        $schema = '
        {
          "type": "number",
          "exclusiveMinimum": true,
          "minimum": 5
        }';
        $data = '4';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Number\ValueNotMultipleOf
     */
    public function multipleOfFault()
    {
        $schema = '
        {
          "type": "number",
          "multipleOf": 3
        }';
        $data = '10';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }
}
