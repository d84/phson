<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Schema\Validator\Validator;

class SchemaBoolTest extends TestCase
{
    /**
     * @test
     */
    public function bool()
    {
        $schema = '
        {
          "type": "boolean"
        }';
        $data = 'true';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedPrimitiveType()
    {
        $schema = '
        {
          "type": "boolean"
        }';
        $data = '1';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedElement()
    {
        $schema = '
        {
          "type": "boolean"
        }';
        $data = '[1]';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }
}
