<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Schema\Validator\Validator;

class SchemaStringTest extends TestCase
{
    /**
     * @test
     */
    public function string()
    {
        $schema = '
        {
          "type": "string"
        }';
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function stringConst()
    {
        $schema = '
        {
          "type": "string",
          "const": "SomeConstant"
        }';
        $data = '"SomeConstant"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\String\StringConstraintException
     */
    public function stringConstException()
    {
        $schema = '
        {
          "type": "string",
          "const": "SomeConstant"
        }';
        $data = '"Hello"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function maxLength()
    {
        $schema = '
        {
          "type": "string",
          "maxLength": 15
        }';
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function maxLengthIsNegativeException()
    {
        $schema = '
        {
          "type": "string",
          "maxLength": -15
        }';
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function minLength()
    {
        $schema = '
        {
          "type": "string",
          "minLength": 6
        }';
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function minLengthIsNegativeException()
    {
        $schema = '
        {
          "type": "string",
          "minLength": -15
        }';
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\String\ValueLengthMoreThanMax
     */
    public function maxLengthException()
    {
        $schema = '
        {
          "type": "string",
          "maxLength": 5
        }';
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\String\ValueLengthLessThanMin
     */
    public function minLengthException()
    {
        $schema = '
        {
          "type": "string",
          "minLength": 5
        }';
        $data = '"Cat"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function enum()
    {
        $schema = '
        {
          "type": "string",
          "enum": ["one", "two", "three"]
        }';
        $data = '"two"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Enum\ValueOutOfScope
     */
    public function enumFault()
    {
        $schema = '
        {
          "type": "string",
          "enum": ["one", "two", "three"]
        }';
        $data = '"five"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function enumNotArrayException()
    {
        $schema = '
        {
          "type": "string",
          "enum": 1
        }';
        $data = '"two"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function formatDateTime()
    {
        $schema = '
        {
          "type": "string",
          "format": "date-time"
        }';
        $data = '"2015-01-01T01:02:03Z"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function formatIsEmptyException()
    {
        $schema = '
        {
          "type": "string",
          "format": ""
        }';
        $data = '"test"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\String\ValueDoesNotMatchFormat
     */
    public function formatDateTimeException()
    {
        $schema = '
        {
          "type": "string",
          "format": "date-time"
        }';
        $data = '"test"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function pattern()
    {
        $schema = '
        {
          "type": "string",
          "pattern": "/^expression-[0-9]$/"
        }';
        $data = '"expression-1"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\String\ValueDoesNotMatchPattern
     */
    public function patternNotMatchException()
    {
        $schema = '
        {
          "type": "string",
          "pattern": "/^expression-[0-9]$/"
        }';
        $data = '"expression-19"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function patternIsEmptyException()
    {
        $schema = '
        {
          "type": "string",
          "pattern": ""
        }';
        $data = '"expression-19"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function formatDate()
    {
        $schema = '
        {
          "type": "string",
          "format": "date"
        }';
        $data = '"2015-01-01"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\String\ValueDoesNotMatchFormat
     */
    public function formatDateException()
    {
        $schema = '
        {
          "type": "string",
          "format": "date"
        }';
        $data = '"test!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function formatEmail()
    {
        $schema = '
        {
          "type": "string",
          "format": "email"
        }';
        $data = '"pave@mail.com"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\String\ValueDoesNotMatchFormat
     */
    public function formatEmailException()
    {
        $schema = '
        {
          "type": "string",
          "format": "email"
        }';
        $data = '"pave[@]1.com"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function formatUriFile()
    {
        $schema = '
        {
          "type": "string",
          "format": "uri"
        }';
        $data = '"file://test.json"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function formatUriHttp()
    {
        $schema = '
        {
          "type": "string",
          "format": "uri"
        }';
        $data = '"http://test.json"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function formatIP()
    {
        $schema = '
        {
          "type": "string",
          "format": "ip"
        }';
        $data = '"192.168.1.1"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }


    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\String\ValueDoesNotMatchFormat
     */
    public function formatIPException()
    {
        $schema = '
            {
              "type": "string",
              "format": "ip"
            }';
        $data = '"192.168.11111.1"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\String\StringConstraintException
     */
    public function formatUnknownException()
    {
        $schema = '
            {
              "type": "string",
              "format": "some-unknown-format"
            }';
        $data = '"192.168.11111.1"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }
}
