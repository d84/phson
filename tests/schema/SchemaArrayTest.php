<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Schema\Validator\Validator;

class SchemaArrayTest extends TestCase
{
    /**
     * @test
     */
    public function array()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "number"
          }
        }';
        $data = '[1,2,3,4,5,6]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function notArrayException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          }
        }';
        $data = '{"name": "Jhon", "age": 44}';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function arrayItemsNotArrayAndNotObjectException()
    {
        $schema = '
        {
          "type": "array",
          "items": 5
        }';
        $data = '[1,2,3]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }


    /**
     * The given array doesn't conform to the schema because it contains a excess element (5)
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\TArray\ArrayConstraintException
     */
    public function arraySizeException()
    {
        $schema = '
        {
          "type": "array",
          "items": [
            {
              "type": "number"
            },
            {
              "type": "boolean"
            },
            {
              "type": "string"
            }
          ]
        }';
        $data = '[1,false,"the string",5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\TArray\ArrayConstraintException
     */
    public function arrayUnexpectedElementException()
    {
        $schema = '
        {
          "type": "array",
          "items": [
            {
              "type": "number"
            },
            {
              "type": "string"
            },
            {
              "type": "number"
            }
          ]
        }';
        $data = '[1,2,3]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function arrayOfInegerOrString()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": ["integer", "string"]
          }
        }';
        $data = '[1,"two",3]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function arrayOfInegerStringInteger()
    {
        $schema = '
        {
          "type": "array",
          "items": [
            {
              "type": "integer"
            },
            {
              "type": "string"
            },
            {
              "type": "integer"
            }
          ]
        }';
        $data = '[1,"two",3]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * Expected array of integers or string, but there is one float value.
     *
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\TArray\ArrayConstraintException
     */
    public function arrayOfInegerOrStringException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": ["integer", "string"]
          }
        }';
        $data = '[1,"two",3,5.6]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function arrayWithAdditionalItems()
    {
        $schema = '
        {
          "type": "array",
          "items": [
            {
              "type": "integer"
            },
            {
              "type": "string"
            }
          ],
          "additionalItems": {
            "type": "object",
            "properties": {
              "temperature": {"type": "float"}
            }
          }
        }';
        $data = '[1,"two",{"temperature": 1.2},{"temperature": 5.9}, {"temperature": 3.0}]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\TArray\ArrayConstraintException
     */
    public function arrayWithAdditionalItemsException()
    {
        $schema = '
        {
          "type": "array",
          "items": [
            {
              "type": "integer"
            },
            {
              "type": "string"
            }
          ],
          "additionalItems": {
            "type": "object",
            "properties": {
              "temperature": {"type": "float"}
            }
          }
        }';
        $data = '[1,"two", 3]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\TArray\ArrayLengthLessThanMin
     */
    public function arrayMinSizeException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          },
          "minItems": 10
        }';
        $data = '[1,2,3,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\TArray\ArrayLengthMoreThanMax
     */
    public function arrayMaxSizeException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          },
          "maxItems": 3
        }';
        $data = '[1,2,3,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function arrayMaxItemsNotIntegerException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          },
          "maxItems": "five"
        }';
        $data = '[1,2,3,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function arrayMaxItemsIsNegativeException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          },
          "maxItems": -67
        }';
        $data = '[1,2,3,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function arrayMinItemsIsNegativeException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          },
          "minItems": -67
        }';
        $data = '[1,2,3,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function getContains()
    {
        $schema = '
        {
          "type": "array",
          "contains": {
            "type": "integer"
          }
        }';
        $data = '[1,2,3,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function arrayUnique()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          },
          "maxItems": 5,
          "uniqueItems": true
        }';
        $data = '[1,2,3,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\TArray\ArrayNotUnique
     */
    public function arrayNonUniqueException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": "integer"
          },
          "maxItems": 5,
          "uniqueItems": true
        }';
        $data = '[1,2,4,4,5]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function arrayNonUniqueObjectException()
    {
        $schema = '
        {
          "type": "array",
          "items": {
            "type": ["integer", "object"],
            "properties": {
              "digit": { "type": "integer" }
            }
          },
          "maxItems": 7,
          "uniqueItems": true
        }';
        $data = '[1,2,3,4,5,{"digit": 1}, {"digit": 2}]';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }
}
