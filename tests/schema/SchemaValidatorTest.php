<?php
use PHPUnit\Framework\TestCase;
use d84\Phson\Schema\Validator\Validator;
use d84\Phson\Document\Element\JsonPrimitive;

class SchemaValidatorTest extends TestCase
{
    /**
     * @test
     */
    public function schemaAsObject()
    {
        $schema = '{
          "type": "string"
        }';
        $schema = json_decode($schema);
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function dataAsJsonElement()
    {
        $schema = '{
          "type": "string"
        }';
        $schema = json_decode($schema);
        $data = new JsonPrimitive("Hello!");

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function schemaMustBeJsonObjectException()
    {
        $schema = '[1]';
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function schemaToJsonDocumentException()
    {
        $schema = 5;
        $data = '"Hello, World!"';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }
}
