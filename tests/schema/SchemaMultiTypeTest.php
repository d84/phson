<?php
use PHPUnit\Framework\TestCase;
use d84\Phson\Schema\Validator\Validator;

class SchemaMultiTypeTest extends TestCase
{
    /**
     * @test
     */
    public function integer()
    {
        $schema = '
        {
          "type": ["float", "integer"]
        }';
        $data = '9';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function float()
    {
        $schema = '
        {
          "type": ["integer", "float"]
        }';
        $data = '9.99';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function expectedIntegerOrFloatButStringSupplied()
    {
        $schema = '
        {
          "type": ["integer", "float"]
        }';
        $data = '"test string"';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function notDefinedTypeKeyword()
    {
        $schema = '
        {
          "properties": {
              "name": {"type": "string"},
              "age": {"type": "integer"}
            }
        }';
        $data = '{"name": "Jhon", "age": 45}';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function wrongTypeForTypeKeyword()
    {
        $schema = '
        {
          "type": 4,
          "properties": {
              "name": {"type": "string"},
              "age": {"type": "integer"}
            }
        }';
        $data = '{"name": "Jhon", "age": 45}';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }
}
