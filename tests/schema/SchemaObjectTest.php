<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Schema\Validator\Validator;

class SchemaObjectTest extends TestCase
{
    /**
     * @test
     */
    public function object()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "p1": {"type": "string"},
            "p2": {"type": "number"},
            "p3": {"type": "string", "enum": ["apple", "orange", "kiwi"]},
            "p4": {
              "type": "object",
              "properties": {
                "t1": {"type": "string", "format": "uri"}
              }
            }
          },
          "required": ["p1", "p3"]
        }';
        $data = '{
          "p1": "Hello!",
          "p2": 12.99,
          "p3": "apple",
          "p4": {
            "t1": "file://c:/photos/album/1"
          }
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function minProperties()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "p1": {"type": "string"},
            "p2": {"type": "number"},
            "p3": {"type": "string", "enum": ["apple", "orange", "kiwi"]}
          },
          "minProperties": 3
        }';
        $data = '{
          "p1": "Hello!",
          "p2": 12.99,
          "p3": "apple"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Object\ObjectConstraintException
     */
    public function minPropertiesException()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "p1": {"type": "string"},
            "p2": {"type": "number"},
            "p3": {"type": "string", "enum": ["apple", "orange", "kiwi"]}
          },
          "minProperties": 3
        }';
        $data = '{
          "p1": "Hello!",
          "p3": "apple"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function minPropertiesIsNegativeException()
    {
        $schema = '
        {
          "type": "object",
          "minProperties": -3
        }';
        $data = '{
          "p1": "Hello!",
          "p3": "apple"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function maxProperties()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "p1": {"type": "string"},
            "p2": {"type": "number"},
            "p3": {"type": "string", "enum": ["apple", "orange", "kiwi"]}
          },
          "maxProperties": 3
        }';
        $data = '{
          "p1": "Hello!",
          "p2": 12.99,
          "p3": "apple"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Object\ObjectConstraintException
     */
    public function maxPropertiesException()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "p1": {"type": "string"},
            "p2": {"type": "number"},
            "p3": {"type": "string", "enum": ["apple", "orange", "kiwi"]}
          },
          "maxProperties": 3
        }';
        $data = '{
          "p1": "Hello!",
          "p3": "apple",
          "p2": 1,
          "p4": 2
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function maxPropertiesIsNegativeException()
    {
        $schema = '
        {
          "type": "object",
          "maxProperties": -1
        }';
        $data = '{
          "p1": "Hello!",
          "p3": "apple",
          "p2": 1,
          "p4": 2
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedElement()
    {
        $schema = '
        {
          "type": "object"
        }';
        $data = '[1]';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function additionalPropertiesException()
    {
        $schema = '
        {
          "type": "object",
          "additionalProperties": 2
        }';
        $data = '{"name": "Jhon"}';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function requiredException()
    {
        $schema = '
        {
          "type": "object",
          "required": 2
        }';
        $data = '{"name": "Jhon"}';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Object\NotFoundRequiredProperty
     */
    public function notFoundRequiredException()
    {
        $schema = '
        {
          "type": "object",
          "required": ["sename"]
        }';
        $data = '{"name": "Jhon"}';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function propertiesNotObjectException()
    {
        $schema = '
        {
          "type": "object",
          "properties": 2
        }';
        $data = '{
          "p1": "Hello!"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function patternProperties()
    {
        $schema = '
        {
          "type": "object",
          "patternProperties": {
            "/^prop-string-[0-9]/": {"type": "string"},
            "/^prop-integer-[0-9]/": {"type": "integer"}
          }
        }';
        $data = '{
          "prop-string-1": "monday!",
          "prop-string-2": "tuesday!",
          "prop-string-3": "wednesday!",
          "prop-integer-1": 100,
          "prop-integer-2": 101,
          "prop-integer-3": 103
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function patternPropertiesTypeException()
    {
        $schema = '
        {
          "type": "object",
          "patternProperties": 2
        }';
        $data = '{
          "prop-string-1": "monday!",
          "prop-string-2": "tuesday!",
          "prop-string-3": "wednesday!",
          "prop-integer-1": 100,
          "prop-integer-2": 101,
          "prop-integer-3": 103
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function additionalProperties()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "name": {"type": "string"},
            "sename": {"type": "string"}
          },
          "patternProperties": {
            "/^extra-[0-9]/": {"type": "string"}
          },
          "additionalProperties": {
            "type": "number"
          },
          "required": ["name", "sename", "extra-0", "subscriber"]
        }';
        $data = '
        {
          "name": "Jhon",
          "sename": "Smith",
          "extra-0": "Primorsky krai",
          "extra-1": "Vladivostok",
          "subscriber": 190009231,
          "key": 90091.34
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Object\ObjectConstraintException
     */
    public function patternPropertyException()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "name": {"type": "string"},
            "sename": {"type": "string"}
          },
          "patternProperties": {
            "/^extra-[0-9]/": {"type": "string"}
          },
          "additionalProperties": {
            "type": "number"
          },
          "required": ["name", "sename", "extra-0", "subscriber"]
        }';
        $data = '
        {
          "name": "Jhon",
          "sename": "Smith",
          "extra-0": "Primorsky krai",
          "extra-1": "Vladivostok",
          "subscriber": 190009231,
          "key": 90091.34,
          "extra-exception": [1,2,3]
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function dependencies()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "name": {"type": "string"},
            "sename": {"type": "string"}
          },
          "dependencies": {
            "name": ["sename"]
          }
        }';
        $data = '
        {
          "name": "Jhon",
          "sename": "Smith"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Object\NotFoundRequiredProperty
     */
    public function dependenciesException()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "name": {"type": "string"},
            "sename": {"type": "string"}
          },
          "dependencies": {
            "name": ["sename"]
          }
        }';
        $data = '
        {
          "name": "Jhon"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function dependenciesNotArrayException()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "name": {"type": "string"},
            "sename": {"type": "string"}
          },
          "dependencies": {
            "name": "sename"
          }
        }';
        $data = '
        {
          "name": "Jhon"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\JsonSchemaException
     */
    public function dependenciesNotObjectException()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "name": {"type": "string"},
            "sename": {"type": "string"}
          },
          "dependencies": "sename"
        }';
        $data = '
        {
          "name": "Jhon"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function ref()
    {
        $schema = '
        {
          "type": "object",
          "properties": {
            "name": {"type": "string"},
            "sename": {"type": "string"},
            "billing_address": {"$ref": "#/definitions/address"},
            "address": {"$ref": "#/definitions/address"}
          },
          "definitions": {
            "address": {
              "type": "string",
              "maxLength": 64
            }
          }
        }';
        $data = '
        {
          "name": "Jhon",
          "sename": "Smith",
          "billing_address": "PrimorskyKrai, Vladivostok",
          "address": "PrimorskyKrai, Khabarovsk"
        }';

        $validator = new Validator();
        $validator->validate($data, $schema);
        $this->assertTrue(true);
    }
}
