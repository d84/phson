<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Schema\Validator\Validator;

class SchemaFloatTest extends TestCase
{
    /**
     * @test
     */
    public function float()
    {
        $schema = '
        {
          "type": "float"
        }';
        $data = '9.99';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedElement()
    {
        $schema = '
        {
          "type": "float"
        }';
        $data = '[9.99]';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedPrimitiveType()
    {
        $schema = '
        {
          "type": "float"
        }';
        $data = '"9.99"';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /*
    --------------------------------------------------------------------------------------------------------------------
    checkMaximum
    --------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function checkMaximumSuccess()
    {
        $schema = '
        {
          "type": "float",
          "maximum": 9.99
        }';
        $data = '9.98';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkMaximumFault()
    {
        $schema = '
        {
          "type": "float",
          "maximum": 9.98
        }';
        $data = '9.99';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function checkExclusiveMaximumSuccess()
    {
        $schema = '
        {
          "type": "float",
          "maximum": 9.99,
          "exclusiveMaximum": true
        }';
        $data = '9.99';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkExclusiveMaximumFault()
    {
        $schema = '
        {
          "type": "float",
          "maximum": 9.99,
          "exclusiveMaximum": true
        }';
        $data = '10.0';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /*
    --------------------------------------------------------------------------------------------------------------------
    checkMinimum
    --------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function checkMinimumSuccess()
    {
        $schema = '
        {
          "type": "float",
          "minimum": 9.1
        }';
        $data = '9.2';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkMinimumFault()
    {
        $schema = '
        {
          "type": "float",
          "minimum": 9.1
        }';
        $data = '9.0';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     */
    public function checkExclusiveMinimumSuccess()
    {
        $schema = '
        {
          "type": "float",
          "minimum": 9.99,
          "exclusiveMinimum": true
        }';
        $data = '9.99';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkExclusiveMinimumFault()
    {
        $schema = '
        {
          "type": "float",
          "minimum": 9.99,
          "exclusiveMinimum": true
        }';
        $data = '9.98';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /*
    --------------------------------------------------------------------------------------------------------------------
    multipleOf
    --------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function multipleOf()
    {
        $schema = '
        {
          "type": "float",
          "multipleOf": 2.0
        }';
        $data = '10.0';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Number\ValueNotMultipleOf
     */
    public function multipleOfFault()
    {
        $schema = '
        {
          "type": "float",
          "multipleOf": 3.0
        }';
        $data = '10.0';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }
}
