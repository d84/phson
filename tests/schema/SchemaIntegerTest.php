<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Schema\Validator\Validator;

class SchemaIntegerTest extends TestCase
{
    /**
     * @test
     */
    public function integer()
    {
        $schema = '
        {
          "type": "integer"
        }';
        $data = '123';

        $validator = new Validator();

        $validator->validate($data, $schema);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedElement()
    {
        $schema = '
        {
          "type": "integer"
        }';
        $data = '[123]';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\ConstraintException
     */
    public function unexpectedPrimitiveType()
    {
        $schema = '
        {
          "type": "integer"
        }';
        $data = '33.10';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkMaximumFault()
    {
        $schema = '
        {
          "type": "integer",
          "maximum": 5
        }';
        $data = '5';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkExclusiveMaximumFault()
    {
        $schema = '
        {
          "type": "integer",
          "exclusiveMaximum": true,
          "maximum": 5
        }';
        $data = '123';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkMinimumFault()
    {
        $schema = '
        {
          "type": "integer",
          "minimum": 5
        }';
        $data = '5';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException  d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange
     */
    public function checkExclusiveMinimumFault()
    {
        $schema = '
        {
          "type": "integer",
          "exclusiveMinimum": true,
          "minimum": 5
        }';
        $data = '3';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }

    /**
     * @test
     * @expectedException d84\Phson\Schema\Exception\Constraint\Number\ValueNotMultipleOf
     */
    public function multipleOfFault()
    {
        $schema = '
        {
          "type": "integer",
          "multipleOf": 5
        }';
        $data = '6';

        $validator = new Validator();

        $validator->validate($data, $schema);
    }
}
