<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Document\Element\JsonElementAbstract;

class JsonElementTest extends TestCase
{
    /**
     * @test
     */
    public function wrapInteger()
    {
        $el = JsonElementAbstract::wrap(1);
        $this->assertEquals(1, $el->asInteger());
    }

    /**
     * @test
     */
    public function wrapNull()
    {
        $el = JsonElementAbstract::wrap(null);
        $this->assertNull($el->getRaw());
    }

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\JsonElementException
     */
    public function unwrapable()
    {
        $fp = fopen("php://temp", 'r+');
        JsonElementAbstract::wrap($fp);
    }

    /**
     * @test
     */
    public function isSupportedValueInteger()
    {
        $this->assertTrue(JsonElementAbstract::isSupportedValue(1));
    }

    /**
     * @test
     */
    public function isSupportedValueNull()
    {
        $this->assertTrue(JsonElementAbstract::isSupportedValue(null));
    }

    /**
     * @test
     */
    public function isSupportedValueArray()
    {
        $this->assertTrue(JsonElementAbstract::isSupportedValue([]));
    }

    /**
     * @test
     */
    public function isNotSupportedValue()
    {
        $fp = fopen("php://temp", 'r+');
        $this->assertFalse(JsonElementAbstract::isSupportedValue($fp));
    }
}
