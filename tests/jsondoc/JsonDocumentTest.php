<?php
use d84\Phson\Document\JsonDocument;
use d84\Phson\Document\JsonObjectMap;
use d84\Phson\Document\JsonDeserializable;
use PHPUnit\Framework\TestCase;

class Address implements JsonSerializable, JsonDeserializable
{
    private $state;
    private $city;
    private $zip;

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function jsonSerialize()
    {
        return [
          'a_state' => $this->state,
          'a_zip' => $this->zip
        ];
    }

    public function jsonDeserialize()
    {
        return [
          'a_state' => 'state',
          'a_zip' => 'zip',
          'a_city' => 'city'
        ];
    }
}

class AddressStore implements JsonSerializable, JsonDeserializable
{
    private $id;
    private $addresses = [];

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
    }

    public function getAddresses()
    {
        return $this->addresses;
    }

    public function remove($index)
    {
        unset($this->addresses[$index]);
    }

    public function jsonSerialize()
    {
        return [
          'address_store_id' => $this->id,
          'address_array' => $this->addresses
        ];
    }

    public function jsonDeserialize()
    {
        return [
          'address_store_id' => 'id',
          'address_array' => 'addresses'
        ];
    }
}

class Producer
{
    private $id;
    private $name;
    private $address;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        return $this->address;
    }
}

class Car
{
    private $model;
    private $vin;
    private $engine;
    private $producer;

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setVin($vin)
    {
        $this->vin = $vin;
    }

    public function getVin()
    {
        return $this->vin;
    }

    public function setEngine($engine)
    {
        $this->engine = $engine;
    }

    public function getEngine()
    {
        return $this->engine;
    }

    public function setProducer($producer)
    {
        $this->producer = $producer;
    }

    public function getProducer()
    {
        return $this->producer;
    }
}

class JsonDocumentTest extends TestCase
{
    /**
     * @test
     */
    public function fromJsonResolveObject()
    {
        $json = '
        {
          "model": "Sportage",
          "vin": 12344321233,
          "engine": "v4"
        }
        ';

        $jdoc = new JsonDocument();
        $jdoc->setObjectMap(new JsonObjectMap());

        $car = $jdoc->fromJson($json, Car::class);

        $this->assertEquals('Sportage', $car->getModel());
    }

    /**
     * @test
     */
    public function fromJsonResolveNestedObjects()
    {
        $json = '
        {
          "model": "Sportage",
          "vin": 12344321233,
          "engine": "v4",
          "producer": {
            "id": 25000231,
            "name": "KIA",
            "address": {
              "state": "Primorsky-Krai",
              "city": "Vladivostok",
              "zip": 690091
            }
          }
        }
        ';

        $jdoc = new JsonDocument();

        $car = $jdoc->fromJson($json, Car::class, ['producer' => Producer::class, 'address' => Address::class]);

        $this->assertEquals(25000231, $car->getProducer()->getId());
        $this->assertEquals('Vladivostok', $car->getProducer()->getAddress()->getCity());
    }

    /**
     * @test
     */
    public function fromJsonResolveArray()
    {
        $json = '
        [
          {
            "state": "Primorsky-Krai",
            "city": "Vladivostok",
            "zip": 690091
          },
          {
            "state": "Primorsky-Krai",
            "city": "Nakhodka",
            "zip": 510091
          },
          {
            "state": "Khabarovky-Krai",
            "city": "Khabarovsk",
            "zip": 930022
          },
          {
            "state": "Khabarovky-Krai",
            "city": "Komsomolsk-Na-Amure",
            "zip": 930043
          }
        ]
        ';

        $jdoc = new JsonDocument();
        $jdoc->setObjectMap(new JsonObjectMap());

        $address_array = $jdoc->fromJson($json, Address::class);

        $this->assertCount(4, $address_array);
        $this->assertEquals('Nakhodka', $address_array[1]->getCity());
    }

    /**
     * @test
     */
    public function toJsonSerializable()
    {
        $address = new Address();
        $address->setState('Primorsky');
        $address->setCity('Vladivostok');
        $address->setZip(690091);

        $json = JsonDocument::toJsonString($address);

        $this->assertEquals('{"a_state":"Primorsky","a_zip":690091}', $json);
    }

    /**
     * @test
     */
    public function toJsonSerializeDeserialize()
    {
        $address = new Address();
        $address->setState('Primorsky');
        $address->setCity('Vladivostok');
        $address->setZip(690091);

        $json = JsonDocument::toJsonString($address);

        $this->assertEquals('{"a_state":"Primorsky","a_zip":690091}', $json);

        $jdoc = new JsonDocument();

        $restored_address = $jdoc->fromJson($json, Address::class);

        $this->assertEquals(690091, $restored_address->getZip());
    }

    /**
     * @test
     */
    public function fromJsonResolveObjectWithArray()
    {
        $json = '
        {
          "address_store_id": 3310,
          "address_array":
          [
            {
              "a_state": "Primorsky-Krai",
              "a_city": "Vladivostok",
              "a_zip": 690091
            },
            {
              "a_state": "Primorsky-Krai",
              "a_city": "Nakhodka",
              "a_zip": 510091
            },
            {
              "a_state": "Khabarovky-Krai",
              "a_city": "Khabarovsk",
              "a_zip": 930022
            },
            {
              "a_state": "Khabarovky-Krai",
              "a_city": "Komsomolsk-Na-Amure",
              "a_zip": 930043
            }
          ]
        }
        ';

        $pson = new JsonDocument();
        $astore = $pson->fromJson($json, AddressStore::class, ['address_array' => Address::class]);
        $this->assertEquals(true, is_array($astore->getAddresses()));

        // Check first element of an array
        $addresses = $astore->getAddresses();
        $this->assertInstanceOf(Address::class, $addresses[0]);

        // Check zip
        $this->assertEquals(690091, $addresses[0]->getZip());

        // Change array of addresses
        $astore->setAddresses([$addresses[0]]);

        // Check serialize
        $this->expectOutputString('{"address_store_id":3310,"address_array":[{"a_state":"Primorsky-Krai","a_zip":690091}]}');
        echo json_encode($astore);
    }
}
