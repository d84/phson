<?php
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Document\Element\JsonArray;

use PHPUnit\Framework\TestCase;
use Carbon\Carbon;

class UserModel
{
    private $login;
    private $id;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getLogin()
    {
        return $this->login;
    }
}

class JsonObjectTest extends TestCase
{
    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\UnexpectedElementException
     */
    public function createFault()
    {
        $json_obj = new JsonObject('{test}');
    }

    /**
     * @test
     */
    public function getPropertiesCount()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));

        $this->assertEquals(4, $json_obj->getPropertiesCount());
    }

    /**
     * @test
     */
    public function getPropertiesKeys()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));

        $keys = $json_obj->getPropertyKeys();

        $this->assertEquals(true, in_array('login', $keys));
    }

    /**
     * @test
     */
    public function isEmptyTest()
    {
        $json_obj = new JsonObject();

        $this->assertEquals(true, $json_obj->isEmpty());
    }

    /**
     * @test
     */
    public function setPropertyElementInt()
    {
        $json_obj = new JsonObject();
        $json_obj->set('age', new JsonPrimitive(33));

        $this->assertEquals(33, $json_obj->asInteger('age'));
    }

    /**
     * @test
     */
    public function asDateFault()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('flag', false);

        $this->assertFalse($json_obj->asDate('flag'));
    }

    /**
     * @test
     */
    public function setPropertyElementString()
    {
        $json_obj = new JsonObject();
        $json_obj->set('name', new JsonPrimitive('Ivan'));

        $this->assertEquals('Ivan', $json_obj->asString('name'));
    }

    /**
     * @test
     */
    public function setPropertyElementFloat()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('price', 12.99);

        $this->assertEquals(12.99, $json_obj->get('price')->asFloat());
    }

    /**
     * @test
     */
    public function setPropertyElementBool()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', true);

        $this->assertEquals(true, $json_obj->get('isAdmin')->asBoolean());
    }

    /**
     * @test
     */
    public function setProperty()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));

        $this->assertEquals(true, $json_obj->get('isAdmin')->asBoolean());
        $this->assertEquals('Cesar', $json_obj->get('login')->asString());
        $this->assertEquals(22, $json_obj->asInteger('age'));
        $this->assertEquals(164.232, $json_obj->asFloat('consumedSpeed'));
    }

    /**
     * @test
     */
    public function removeProperty()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));

        $json_old = $json_obj->remove('login');

        $this->assertEquals('Cesar', $json_old->asString());
        $this->assertFalse($json_obj->has('login'));
    }

    /**
     * @test
     */
    public function getPropertyArrayAsString()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('numbers', new JsonArray([1,2,3,4,5,6,7,8,9]));

        $this->assertEquals('1,2,3,4,5,6,7,8,9', $json_obj->asString('numbers'));
    }

    /**
     * @test
     */
    public function iterateProperties()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));

        $iterator = $json_obj->getIterator();

        foreach ($iterator as $name => $value) {
            $this->assertArrayHasKey($name, ['isAdmin' => 1, 'login' => 2, 'age' => 3, 'consumedSpeed' => 4]);
        }
    }

    /**
     * @test
     */
    public function getWrappedEntrySet()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));

        // entrySet() returns assoc array of the JsonElementAbstract elements
        $entries = $json_obj->entrySet();

        foreach ($entries as $name => $entry) {
            if ($name === 'login') {
                $this->assertEquals('Cesar', $entry->asString());
            }
        }
    }

    /**
     * @test
     */
    public function getEntrySet()
    {
        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));

        // entrySet() returns assoc array of the native elements (NOT WRAPPED)
        $entries = $json_obj->entrySet(false);

        foreach ($entries as $name => $entry) {
            if ($name === 'login') {
                $this->assertEquals('Cesar', $entry);
            }
        }
    }

    /**
     * @test
     */
    public function findPathOk()
    {
        $json_obj_address = new JsonObject();
        $json_obj_address->setProperty('street', 'Pologaya');
        $json_obj_address->setProperty('unit', 19);

        $json_obj_location = new JsonObject();
        $json_obj_location->setProperty('country', 'Russia');
        $json_obj_location->setProperty('city', 'Vladivostok');
        $json_obj_location->setProperty('zip', '690091');
        $json_obj_location->setProperty('address', $json_obj_address);

        $json_obj = new JsonObject();
        $json_obj->setProperty('isAdmin', new JsonPrimitive(true));
        $json_obj->setProperty('login', new JsonPrimitive('Cesar'));
        $json_obj->setProperty('age', new JsonPrimitive(22));
        $json_obj->setProperty('consumedSpeed', new JsonPrimitive(164.232));
        $json_obj->setProperty('location', $json_obj_location);

        $json_street = $json_obj->find('location/address/street');

        $this->assertInstanceOf(JsonPrimitive::class, $json_street);
        $this->assertEquals('Pologaya', $json_street->asString());
    }

    /**
     * @test
     */
    public function findPathNoPath()
    {
        $json_obj_location = new JsonObject();
        $json_obj_location->setProperty('country', 'Russia');
        $json_obj_location->setProperty('city', 'Vladivostok');
        $json_obj_location->setProperty('zip', '690091');

        $json_street = $json_obj_location->find('city/names/Vladivostok');

        $this->assertFalse($json_street);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function findPathNotStringFault()
    {
        $jobj = new JsonObject();
        $jobj->find(1);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function findPathEmptyFault()
    {
        $jobj = new JsonObject();
        $jobj->find("");
    }

    /**
     * @test
     */
    public function objectFromArray()
    {
        $jobj = JsonObject::fromArray([
          'name' => 'Pavel',
          'sename' => 'Danilin',
          'id' => [
            'number' => 3310,
            'issued' => '13.12.2017 12:23:10'
          ]
        ]);
        $this->assertInstanceOf(Carbon::class, $jobj->get('id')->asDate('issued'));
        $this->assertEquals('13.12.2017 12:23:10', $jobj->find('/id/issued'));
    }

    /**
     * @test
     */
    public function findNoNextField()
    {
        $jobj = JsonObject::fromArray([
          'name' => 'Pavel',
          'sename' => 'Danilin',
          'id' => [
            'number' => 3310,
            'issued' => '13.12.2017 12:23:10'
          ]
        ]);
        $this->assertFalse($jobj->find('/id/stuff/pen'));
    }

    /**
     * @test
     */
    public function findRoot()
    {
        $jobj = JsonObject::fromArray([
          'name' => 'Pavel',
          'sename' => 'Danilin',
          'id' => [
            'number' => 3310,
            'issued' => '13.12.2017 12:23:10'
          ]
        ]);
        $this->assertEquals('Pavel', $jobj->find('/')->asString('name'));
    }

    /**
     * @test
     */
    public function getPropertyDate()
    {
        $json_order = new JsonObject();
        $json_order->setProperty('orderId', 45332323);
        $json_order->setProperty('orderDate', '30.11.2017 09:58:22');
        $json_order->setProperty('userId', 55902333);

        $this->assertEquals(9, $json_order->asDate('orderDate')->hour);
    }

    /**
     * @test
     */
    public function chagePropertyDate()
    {
        $json_order = new JsonObject();
        $json_order->setProperty('orderId', 45332323);
        $json_order->setProperty('orderDate', '30.11.2017 10:58:22');
        $json_order->setProperty('userId', 55902333);

        $order_date = $json_order->asDate('orderDate');

        $this->assertNotFalse($order_date);

        $order_date->addHours(2);
        $json_order->setProperty('orderDate', $order_date);

        $this->assertEquals(12, $json_order->asDate('orderDate')->hour);
    }

    /**
     * @test
     */
    public function wrapModel()
    {
        $user = new UserModel();
        $user->setLogin('Pavel');

        $json_order = new JsonObject($user);

        $this->assertEquals('Pavel', $json_order->asString('login'));
    }

    /**
     * @test
     */
    public function useSetterToChangeObject()
    {
        $json_user = new JsonObject(new UserModel());
        $json_user->setProperty('login', 'Roman');

        $this->assertEquals('Roman', $json_user->asString('login'));
    }

    /**
     * @test
     */
    public function useSetterToChangeObject2()
    {
        $json_user = new JsonObject(new UserModel());
        $json_user->set('login', new JsonPrimitive('Roman'));

        $this->assertEquals('Roman', $json_user->asString('login'));
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function setPropertyFault()
    {
        $json_user = new JsonObject(new UserModel());
        $json_user->setProperty(1, 'Roman');
    }

    /**
     * @test
     */
    public function notEquals()
    {
        $json_user1 = new JsonObject(new UserModel());
        $json_user1->setProperty('login', 'Roman');

        $json_user2 = new JsonObject(new UserModel());
        $json_user2->setProperty('login', 'Pavel');


        $this->assertEquals(false, $json_user1->equals($json_user2));
    }

    /**
     * @test
     */
    public function equals()
    {
        $json_user1 = new JsonObject(new UserModel());
        $json_user1->setProperty('login', 'Pavel');

        $json_user2 = new JsonObject(new UserModel());
        $json_user2->setProperty('login', 'Pavel');


        $this->assertEquals(true, $json_user1->equals($json_user2));
    }

    /**
     * @test
     */
    public function notEqualsByType()
    {
        $json_user1 = new JsonObject(new UserModel());
        $json_user1->setProperty('login', 'Roman');

        $this->assertEquals(false, $json_user1->equals(new JsonPrimitive('Roman')));
    }

    /**
     * @test
     */
    public function setPropertyByOffset()
    {
        $json_user = new JsonObject(new UserModel());
        $json_user['login'] = 'Ivan';

        $this->assertEquals('Ivan', $json_user->asString('login'));
    }

    /**
     * @test
     */
    public function unsetPropertyByOffset()
    {
        $json_user = new JsonObject(new UserModel());
        $json_user['login'] = 'Ivan';
        unset($json_user['login']);

        $this->assertEquals(0, $json_user->getPropertiesCount());
    }
}
