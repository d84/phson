<?php
use PHPUnit\Framework\TestCase;

use d84\Phson\Document\Element\JsonNull;
use d84\Phson\Document\Element\JsonPrimitive;

class JsonNullTest extends TestCase
{
    /**
     * @test
     */
    public function null()
    {
        $el = new JsonNull();
        $this->assertEquals('null', (string)$el);
    }

    /**
     * @test
     */
    public function equals()
    {
        $el = new JsonNull();
        $el1 = new JsonNull();
        $this->assertTrue($el->equals($el1));
    }

    /**
     * @test
     */
    public function notEquals()
    {
        $el = new JsonNull();
        $el1 = new JsonPrimitive(1);
        $this->assertFalse($el->equals($el1));
    }
}
