<?php
use Carbon\Carbon;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Document\Element\JsonArray;
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonNull;
use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Document\JsonDeserializable;
use d84\Phson\Document\JsonDocument;

use PHPUnit\Framework\TestCase;

class FileModel implements JsonSerializable, JsonDeserializable
{
    private $resource_id = null;
    private $owner = null;

    public function setResourceId($resource_id)
    {
        if ($this->resource_id !== null) {
            throw new \RuntimeException('ResourceId is immutable!');
        }
        $this->resource_id = $resource_id;
    }

    public function getResourceId()
    {
        return $this->resource_id;
    }

    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function jsonSerialize()
    {
        return [
          'resourceId' => $this->resource_id,
          'ownerUser' => $this->owner
        ];
    }

    public function jsonDeserialize()
    {
        return [
          'resourceId' => 'resourceId', // Will use setter
          'ownerUser' => 'owner'
        ];
    }
}

class JsonArrayTest extends TestCase
{
    /**
     * @test
     */
    public function createArray()
    {
        $json_arr = new JsonArray([new JsonPrimitive(1), new JsonPrimitive(2)]);
        $this->assertEquals([1,2], $json_arr->getAll(false));
    }

    /**
     * @test
     */
    public function indexAccess()
    {
        $json_arr = new JsonArray(['car', 'plane', 'train']);

        if (isset($json_arr[1])) {
            $this->assertEquals('plane', $json_arr[1]->asString());
        }

        $json_arr[1] = 'helicopter';
        $this->assertEquals('helicopter', $json_arr[1]->asString());

        unset($json_arr[1]);
    }

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\UnexpectedElementException
     */
    public function integerNotArrayException()
    {
        $json_arr = new JsonArray(1);
    }

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\UnexpectedElementException
     */
    public function stringNotArrayException()
    {
        $json_arr = new JsonArray('Hello!');
    }

    /**
     * @test
     */
    public function aceessByIndexArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        // $json_arr[1] - JsonPrimitive
        $this->assertEquals(2, $json_arr[1]->asInteger());
    }

    /**
     * @test
     */
    public function addPrimitiveToArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr->add(500);
        // $json_arr[5] - JsonPrimitive
        $this->assertEquals(500, $json_arr[5]->asInteger());
    }

    /**
     * @test
     */
    public function addObjectToArray()
    {
        $test_obj = new \stdClass();
        $test_obj->id = 9990;

        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr->add($test_obj);
        // $json_arr[5] - JsonObject
        $this->assertEquals(9990, $json_arr[5]->asInteger('id'));
    }

    /**
     * @test
     */
    public function addElementToArray()
    {
        $test_obj = new JsonObject();
        $test_obj->setProperty('id', 888);

        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr->add($test_obj);
        // $json_arr[5] - JsonObject
        $this->assertEquals(888, $json_arr[5]->asInteger('id'));
    }

    /**
     * @test
     */
    public function addCarbonElementToArray()
    {
        $date_obj = new Carbon('08.12.2017');

        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr->add($date_obj);

        $this->assertEquals(12, $json_arr[5]->asDate()->month);
    }

    /**
     * @test
     */
    public function mergeArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr2 = new JsonArray([6,7,8,9]);
        $new_array = $json_arr->merge($json_arr2);

        $this->assertEquals(9, $new_array[8]->asInteger());
    }

    /**
     * @test
     */
    public function addAllArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr2 = new JsonArray([6,7,8,'Hello!']);
        $json_arr->addAll($json_arr2);

        $this->assertEquals('Hello!', $json_arr[8]->asString());
    }

    /**
     * @test
     */
    public function setDate()
    {
        $json_arr = new JsonArray([1]);
        $json_arr->set(0, new Carbon('08.12.2017 11:22'));

        $this->assertEquals(11, $json_arr[0]->asDate()->hour);
    }

    /**
     * @test
     */
    public function setJsonElementAbstract()
    {
        $json_arr = new JsonArray([1]);
        $json_arr->set(0, new JsonPrimitive(33));

        $this->assertEquals(33, $json_arr[0]->asInteger());
    }


    /**
     * @test
     */
    public function setNotExistElement()
    {
        $json_arr = new JsonArray([1]);

        $this->assertEquals(false, $json_arr->set(100, 4));
    }

    /**
     * @test
     */
    public function setAllArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr2 = new JsonArray(['one', 'two', 'three']);
        $json_arr->setAll($json_arr2);

        $this->assertEquals('two', $json_arr->get(1)->asString());
    }

    /**
     * @test
     */
    public function getAllUnwrapped()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);

        $this->assertEquals([1,2,3,4,5], $json_arr->getAll(false));
    }

    /**
     * @test
     */
    public function getAllWrapped()
    {
        $json_arr = new JsonArray([1,2]);

        $this->assertEquals([new JsonPrimitive(1), new JsonPrimitive(2)], $json_arr->getAll());
    }

    /**
     * @test
     */
    public function getLast()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        $this->assertEquals(5, $json_arr->last()->asInteger());
    }

    /**
     * @test
     */
    public function getLastNone()
    {
        $json_arr = new JsonArray([]);
        $this->assertEquals(null, $json_arr->last());
    }

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\JsonElementException
     */
    public function getFault()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        $json_arr->get('1')->asInteger();
    }

    /**
     * @test
     */
    public function getDefault()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);
        $this->assertEquals('NoN', $json_arr->get(100, true, 'NoN'));
    }

    /**
     * @test
     */
    public function getWrappedByIndexArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);

        $this->assertEquals(true, 3 === $json_arr->get(2)->asInteger());
    }

    /**
     * @test
     */
    public function getUnwrappedByIndexArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);

        $this->assertEquals(true, 3 === $json_arr->get(2, false));
    }

    /**
     * @test
     */
    public function asIntegerByIndexArray()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);

        $this->assertEquals(true, 3 === $json_arr->asInteger(2));
    }

    /**
     * @test
     * @expectedException  d84\Phson\Document\Exception\JsonElementException
     */
    public function asIntegerFault()
    {
        $json_arr = new JsonArray([1,2,3,4,5]);

        $json_arr->asInteger(99, '666');
    }

    /**
     * @test
     */
    public function asIntegerDefault()
    {
        $json_arr = new JsonArray([1,2,3,4,5, [1]]);

        $this->assertEquals(666, $json_arr->asInteger(99, 666));
        $this->assertEquals(777, $json_arr->asInteger(5, 777));
    }

    /**
     * @test
     */
    public function asStringByIndexArray()
    {
        $json_arr = new JsonArray(['apple', 'orange', 'banana']);

        $this->assertEquals('orange', $json_arr->asString(1));
    }

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\JsonElementException
     */
    public function asStringFault()
    {
        $json_arr = new JsonArray(['apple', 'orange', 'banana']);

        $json_arr->asString(1, 9.9);
    }

    /**
     * @test
     */
    public function asStringDefault()
    {
        $json_arr = new JsonArray(['apple', 'orange', 'banana', [1]]);

        $this->assertEquals('plane', $json_arr->asString(999, 'plane'));
    }

    /**
     * @test
     */
    public function asFloatByIndexArray()
    {
        $json_arr = new JsonArray([4.3, 5.1, 6.44]);

        $this->assertEquals(true, 6.44 === $json_arr->asFloat(2));
    }

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\JsonElementException
     */
    public function asFloatFault()
    {
        $json_arr = new JsonArray([4.3, 5.1, 6.44]);

        $json_arr->asFloat(99, []);
    }

    /**
     * @test
     */
    public function asFloatDefaulty()
    {
        $json_arr = new JsonArray([4.3, 5.1, 6.44, [5.5]]);

        $this->assertEquals(666, $json_arr->asFloat(99, 666.0));
        $this->assertEquals(777, $json_arr->asFloat(3, 777.0));
    }

    /**
     * @test
     */
    public function asBooleanByIndexArray()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);

        $this->assertEquals(false, $json_arr->asBoolean(2));
    }

    /**
     * @test
     * @expectedException  d84\Phson\Document\Exception\JsonElementException
     */
    public function asBooleanFault()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);
        $json_arr->asBoolean(29, '4');
    }

    /**
     * @test
     */
    public function asBooleanDefault()
    {
        $json_arr = new JsonArray(['Hello!', [1], false]);

        $this->assertEquals(false, $json_arr->asBoolean(99));
        $this->assertEquals(true, $json_arr->asBoolean(1, true));
    }

    /**
     * @test
     */
    public function asNumberByIndexArray()
    {
        $json_arr = new JsonArray([4.3, 1, 6.44]);

        $this->assertEquals(true, 6.44 === $json_arr->asNumber(2));
        $this->assertEquals(true, 1 === $json_arr->asNumber(1));
    }

    /**
     * @test
     */
    public function asNumberDefault()
    {
        $json_arr = new JsonArray([4.3, 1, 6.44, [44]]);

        $this->assertEquals(666, $json_arr->asNumber(999, 666));
        $this->assertEquals(777, $json_arr->asNumber(3, 777));
    }

    /**
     * @test
     * @expectedException  d84\Phson\Document\Exception\JsonElementException
     */
    public function asNumberFault()
    {
        $json_arr = new JsonArray([4.3, 1, 6.44]);

        $json_arr->asNumber(99, '0.0');
    }

    /**
     * @test
     */
    public function removeByIndex()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);

        $old_val = $json_arr->remove(1);

        $this->assertEquals(true, $old_val !== false);
    }

    /**
     * @test
     */
    public function removeByIndexAndCheckValue()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);

        $old_val = $json_arr->remove(1);

        $this->assertEquals(1, $old_val->asInteger());
    }

    /**
     * @test
     */
    public function removeByIndexAndCheckArraySize()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);

        $old_val = $json_arr->remove(2);

        $this->assertEquals(2, $json_arr->size());
    }

    /**
     * @test
     * @expectedException OutOfRangeException
     */
    /*public function checkOutOfRangeIndexException()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);

        $is_exists = $json_arr->get(99999);
    }*/

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\JsonElementException
     */
    public function checkInvalidIndexException()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);

        $is_exists = $json_arr->has('testvalue');
    }

    /**
     * @test
     */
    public function setNewValue()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);
        $old_val = $json_arr->set(1, 'Spring');

        $this->assertEquals('Spring', $json_arr->asString(1));
    }

    /**
     * @test
     */
    public function equalsArray()
    {
        $obj1 = new \stdClass();
        $obj1->id = 33;

        $obj2 = new \stdClass();
        $obj2->id = 33;

        $json_arr1 = new JsonArray(['Hello!', 1, false, $obj1]);
        $json_arr2 = new JsonArray(['Hello!', 1, false, $obj2]);

        $this->assertEquals(true, $json_arr1->equals($json_arr2));
    }

    /**
     * @test
     */
    public function notEqualsArray()
    {
        $el1 = new JsonArray(['one', 'two', 'three']);
        $el2 = new JsonPrimitive(1);

        $this->assertEquals(false, $el1->equals($el2));
    }

    /**
     * @test
     */
    public function notEqualsArrayBySize()
    {
        $el1 = new JsonArray(['one', 'two', 'three']);
        $el2 = new JsonArray(['one', 'two', 'three', 'five!']);

        $this->assertEquals(false, $el1->equals($el2));
    }

    /**
     * @test
     */
    public function equalsMultiDimensionArray()
    {
        $obj1 = new \stdClass();
        $obj1->id = 33;

        $obj2 = new \stdClass();
        $obj2->id = 33;

        $json_arr1 = new JsonArray(['Hello!', 1, false, $obj1, [1,$obj1,3]]);
        $json_arr2 = new JsonArray(['Hello!', 1, false, $obj2, [1,$obj2,3]]);

        $this->assertEquals(true, $json_arr1->equals($json_arr2));
    }

    /**
     * @test
     */
    public function containtsPrimitive()
    {
        $json_arr = new JsonArray(['Hello!', 1, false]);
        $e = new JsonPrimitive(1);

        $this->assertEquals(true, $json_arr->contains($e));
    }

    /**
     * @test
     */
    public function containtsNull()
    {
        $json_arr = new JsonArray(['Hello!', 1, false, null]);
        $e = new JsonNull();

        $this->assertEquals(true, $json_arr->contains($e));
    }

    /**
     * @test
     */
    public function containtsObject()
    {
        $user = new \stdClass();
        $user->name = 'Paul';
        $user->age = 35;

        $json_arr = new JsonArray(['Hello!', 1, $user, false, null]);
        $e = new \stdClass();
        $e->name = 'Paul';
        $e->age = 35;
        $e = JsonElementAbstract::wrap($e);

        $this->assertEquals(true, $json_arr->contains($e));
    }

    /**
     * @test
     */
    public function containtsArray()
    {
        $user = new \stdClass();
        $user->name = 'Paul';
        $user->age = 35;

        $json_arr = new JsonArray(['Hello!', 1, $user, false, null]);

        $sub_arr = [1, $user, false];
        $sub_arr = JsonElementAbstract::wrap($sub_arr);

        $this->assertEquals(true, $json_arr->contains($sub_arr));
    }

    /**
     * @test
     */
    public function notContaintsArray()
    {
        $user = new \stdClass();
        $user->name = 'Paul';
        $user->age = 35;

        $json_arr = new JsonArray(['Hello!', 1, $user, false, null]);

        $user2 = new \stdClass();
        $user2->name = 'Paul';
        $user2->age = 34;

        $sub_arr = [1, $user2, false];
        $sub_arr = JsonElementAbstract::wrap($sub_arr);

        $this->assertEquals(false, $json_arr->contains($sub_arr));
    }

    /**
     * @test
     */
    public function notContaintsObject()
    {
        $user = new \stdClass();
        $user->name = 'Paul';
        $user->age = 35;

        $json_arr = new JsonArray(['Hello!', 1, $user, false, null]);
        $e = new \stdClass();
        $e->name = 'Dima';
        $e->age = 35;
        $e = JsonElementAbstract::wrap($e);

        $this->assertEquals(false, $json_arr->contains($e));
    }

    /**
     * @test
     */
    public function notContainsPrimitive()
    {
        $json_arr = new JsonArray(['Hello!', 1, false, null]);
        $e = new JsonPrimitive(333);

        $this->assertEquals(false, $json_arr->contains($e));
    }

    /**
     * @test
     */
    public function getElementDate()
    {
        $json_arr = new JsonArray(['Hello!', 1, false, null, '30.11.2017 09:58:22']);

        $this->assertEquals(9, $json_arr->asDate(4)->hour);
    }

    /**
     * @test
     */
    public function chageElementDate()
    {
        $json_arr = new JsonArray(['Hello!', 1, false, 1512260580, null]);

        // Timestamp=12/03/2017 @ 12:23am (UTC)
        $order_date = $json_arr->asDate(3, null, 'UTC');

        $this->assertNotFalse($order_date);

        $order_date->addHours(7);
        $json_arr->set(3, $order_date->timestamp);

        //$this->assertEquals(7, $json_arr->asDate(3)->hour);
        $this->assertEquals(23, $json_arr->asDate(3)->minute);
    }

    /**
     * @test
     */
    public function filter()
    {
        $file_1 = new FileModel();
        $file_1->setResourceId(19);
        $file_1->setOwner('Ivan');

        $file_2 = new FileModel();
        $file_2->setResourceId(223);
        $file_2->setOwner('Pavel');

        $file_3 = new FileModel();
        $file_3->setResourceId(665);
        $file_3->setOwner('Ivan');

        $json_arr = new JsonArray([$file_1, $file_2, $file_3]);

        // ---------
        // 1. Filter array
        $file_owner_ivan = $json_arr->filter(function ($value) {
            return $value->getOwner() === 'Ivan';
        });

        // --------
        // 2. Check result
        // Array of two FileModel with owner=Ivan
        $this->assertEquals(2, $file_owner_ivan->size());

        // --------
        // 3. Result to JSON
        $this->expectOutputString("[{\"resourceId\":19,\"ownerUser\":\"Ivan\"},{\"resourceId\":665,\"ownerUser\":\"Ivan\"}]");
        // see JsonArray::__toStirng
        $json_ivan_array = (string)$file_owner_ivan;
        echo $json_ivan_array;

        // --------
        // 4. Restore from JSON to array
        // Restore to array of FileModel elements
        $pson = new JsonDocument();

        // The regular php array of the FileModel objects
        $restored = $pson->fromJson($json_ivan_array, FileModel::class);

        $this->assertInstanceOf(FileModel::class, $restored[1]);
        $this->assertEquals(665, $restored[1]->getResourceId());
        $this->assertEquals('Ivan', $restored[1]->getOwner());
    }

    /**
     * @test
     */
    public function each()
    {
        $json_arr = new JsonArray([2, 3, 3, 7]);
        $sum = 0;
        $json_arr->each(function ($v) use (&$sum) {
            $sum += $v;
        });
        $this->assertEquals(15, $sum);
    }

    /**
     * @test
     */
    public function isEmptyArray()
    {
        $json_arr = new JsonArray();
        $this->assertTrue($json_arr->isEmpty());
    }
}
