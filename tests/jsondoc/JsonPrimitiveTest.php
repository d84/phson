<?php
use PHPUnit\Framework\TestCase;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Document\Element\JsonObject;

class JsonPrimitiveTest extends TestCase
{
    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\UnexpectedElementException
     */
    public function arrayNotPrimitiveException()
    {
        $json_el = new JsonPrimitive([]);
    }

    /**
     * @test
     * @expectedException d84\Phson\Document\Exception\UnexpectedElementException
     */
    public function objectNotPrimitiveException()
    {
        $json_el = new JsonPrimitive(new \stdClass());
    }

    /**
     * @test
     */
    public function intPrimitive()
    {
        $json_int = new JsonPrimitive(123);
        $this->assertEquals(true, $json_int->isInteger());
    }

    /**
     * @test
     */
    public function floatPrimitive()
    {
        $json_float = new JsonPrimitive(12.99);
        $this->assertEquals(true, $json_float->isFloat());
    }

    /**
     * @test
     */
    public function stringPrimitive()
    {
        $json_str = new JsonPrimitive('Hello!');
        $this->assertEquals(true, $json_str->isString());
    }

    /**
     * @test
     */
    public function boolPrimitive()
    {
        $json_bool = new JsonPrimitive(true);
        $this->assertEquals(true, $json_bool->isBoolean());
    }

    /**
     * @test
     */
    public function getFloatasIntegerPrimitive()
    {
        $json_el = new JsonPrimitive(9.99);
        $this->assertEquals(9, $json_el->asInteger());
    }

    /**
     * @test
     */
    public function getStringAsFloatPrimitive()
    {
        $el = new JsonPrimitive('9.99');
        $this->assertEquals(9.99, $el->asFloat());
    }

    /**
     * @test
     */
    public function getStringAsFloatPrimitiveFault()
    {
        $el = new JsonPrimitive('Monday');
        $this->assertEquals(null, $el->asFloat());
    }

    /**
     * @test
     */
    public function getStringasIntegerPrimitive()
    {
        $el = new JsonPrimitive('59.49');
        $this->assertEquals(59, $el->asInteger());
    }

    /**
     * @test
     */
    public function getStringasIntegerPrimitiveFault()
    {
        $el = new JsonPrimitive('Hello!');
        $this->assertEquals(null, $el->asInteger());
    }

    /**
     * @test
     */
    public function getInegerAsStringPrimitive()
    {
        $json_el = new JsonPrimitive(100500);
        $this->assertEquals('100500', $json_el->asString());
    }

    /**
     * @test
     */
    public function getBoolAsStringPrimitive()
    {
        $json_el = new JsonPrimitive(false);
        $this->assertEquals('false', $json_el->asString());
    }

    /**
     * @test
     */
    public function isIntNumber()
    {
        $el = new JsonPrimitive(12);
        $this->assertTrue($el->isNumber());
    }

    /**
     * @test
     */
    public function isFloatNumber()
    {
        $el = new JsonPrimitive(12.99);
        $this->assertTrue($el->isNumber());
    }

    /**
     * @test
     */
    public function stringisNotNumber()
    {
        $el = new JsonPrimitive('123');
        $this->assertFalse($el->isNumber());
    }

    /**
     * @test
     */
    public function getIntAsNumber()
    {
        $el = new JsonPrimitive(123);
        $this->assertEquals(123, $el->asNumber());
    }

    /**
     * @test
     */
    public function getFloatAsNumber()
    {
        $el = new JsonPrimitive(9.99);
        $this->assertEquals(9.99, $el->asNumber());
    }

    /**
     * @test
     */
    public function getStringAsNumber()
    {
        $el = new JsonPrimitive('9.99');
        $this->assertEquals(9.99, $el->asNumber());
    }

    /**
     * @test
     */
    public function getStringAsNumberFault()
    {
        $el = new JsonPrimitive('Boom!');
        $this->assertEquals(null, $el->asNumber());
    }

    /**
     * @test
     */
    public function getBoolAsNumber()
    {
        $el = new JsonPrimitive(false);
        $this->assertEquals(0, $el->asNumber());
    }

    /**
     * @test
     */
    public function getStringAsBool()
    {
        $el = new JsonPrimitive('true');
        $this->assertEquals(true, $el->asBoolean());
    }

    /**
     * @test
     */
    public function getStringAsBoolFault()
    {
        $el = new JsonPrimitive('Hero!');
        $this->assertEquals(null, $el->asBoolean());
    }


    /**
     * @test
     */
    public function equalsPrimitive()
    {
        $json_el1 = new JsonPrimitive(false);
        $json_el2 = new JsonPrimitive(false);
        $this->assertEquals(true, $json_el1->equals($json_el2));
    }

    /**
     * @test
     */
    public function notEqualsByValuePrimitive()
    {
        $json_el1 = new JsonPrimitive(12);
        $json_el2 = new JsonPrimitive(90);
        $this->assertEquals(false, $json_el1->equals($json_el2));
    }

    /**
     * @test
     */
    public function equalsPrimitiveException()
    {
        $json_el1 = new JsonPrimitive(12);
        $json_el2 = new JsonObject();
        $this->assertEquals(false, $json_el1->equals($json_el2));
    }

    /**
     * @test
     */
    public function notEqualsByType2Primitive()
    {
        $json_el1 = new JsonPrimitive(12);
        $json_el2 = new JsonPrimitive('Hello!');
        $this->assertEquals(false, $json_el1->equals($json_el2));
    }

    /**
     * @test
     */
    public function getDate()
    {
        $json_el = new JsonPrimitive('30.11.2017 14:31');

        $this->assertEquals(30, $json_el->asDate()->day);
        $this->assertEquals(2017, $json_el->asDate()->year);
        $this->assertEquals(11, $json_el->asDate()->month);
    }

    /**
     * @test
     */
    public function checkDate()
    {
        $json_el = new JsonPrimitive(55.6);

        $this->assertFalse($json_el->asDate());
    }

    /**
     * @test
     */
    public function isDate()
    {
        $json_el = new JsonPrimitive(44.99);

        $this->assertFalse($json_el->isDate());
    }

    /**
     * @test
     */
    public function getDateAsTimestamp()
    {
        $json_el = new JsonPrimitive(1512019903);

        $this->assertTrue($json_el->isDate());
        $this->assertEquals(11, $json_el->asDate()->month);
    }

    /**
     * @test
     */
    public function getDateFromFormat()
    {
        $json_el = new JsonPrimitive('2017.12.07');

        $this->assertEquals(12, $json_el->asDate('Y.m.d')->month);
    }

    /**
     * @test
     */
    public function primitiveToString()
    {
        $el = new JsonPrimitive(900.12);
        $this->assertEquals('900.12', (string)$el);
    }
}
