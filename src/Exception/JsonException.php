<?php
namespace d84\Phson\Exception;

use RuntimeException;

/**
 * JsonException
 */
class JsonException extends RuntimeException
{
    /**
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
