<?php
namespace d84\Phson\Document;

use InvalidArgumentException;
use d84\Phson\Document\Element\JsonArray;
use d84\Phson\Document\Element\JsonElementAbstract;

/**
 * JsonObjectMap
 */
class JsonObjectMap implements JsonObjectMapInterface
{
    /**
     * @var string
     */
    private $setter_prefix;
    /**
     * @var string
     */
    private $setter_suffix;
    /**
     * @var array Assoc array: key(alias)=>value(class name)
     */
    private $objectmap;

    /**
     * @param array $map
     * @param string $setter_prefix
     * @param string $setter_suffix
     */
    public function __construct(array $map = [], $setter_prefix = 'set', $setter_suffix = '')
    {
        $this->setSetterPrefix($setter_prefix);
        $this->setSetterSuffix($setter_suffix);
        $this->registerAll($map);
    }

    /**
     * Creates instance of the $class and populate it by data from the $element
     *
     * @param  JsonElementAbstract $element
     * @param  string              $class
     * @return object|array       Instance of the particular $class or array of the instances.
     */
    public function resolve(JsonElementAbstract $element, $class)
    {
        if ($element->isObject()) {
            return $this->newInstance($class, get_object_vars($element->getRaw()));
        }
        if ($element instanceof JsonArray) {
            $iterator = $element->getIterator();
            $out_array = [];
            foreach ($iterator as $k => $v) {
                if (is_object($v)) {
                    if ($v instanceof JsonElementAbstract) {
                        $v = $v->getRaw();
                    }
                    $el = $this->newInstance($class, get_object_vars($v));
                    $out_array[$k] = $el;
                } else {
                    $out_array[$k] = $v;
                }
            }
            return $out_array;
        }
        return null;
    }

    /**
     * @param  string $alias
     * @param  string $class
     * @return JsonObjectMapInterface
     */
    public function register($alias, $class)
    {
        $alias = $this->filterAlias($alias);
        $class = $this->filterClass($class);
        $this->objectmap[$alias] = $class;
        return $this;
    }

    /**
     * @param  array $map
     * @return JsonObjectMapInterface
     */
    public function registerAll(array $map)
    {
        $this->clear();
        foreach ($map as $alias => $class) {
            $this->register($alias, $class);
        }
        return $this;
    }

    /**
     * @return JsonObjectMapInterface
     */
    public function clear()
    {
        $this->objectmap = [];
        return $this;
    }

    /**
     * @param  string $prefix
     * @return JsonObjectMapInterface
     */
    public function setSetterPrefix($prefix)
    {
        $this->setter_prefix = $prefix;
        return $this;
    }

    /**
     * @param  string $suffix
     * @return JsonObjectMapInterface
     */
    public function setSetterSuffix($suffix)
    {
        $this->setter_suffix = $suffix;
        return $this;
    }

    /**
     * @param  string $class
     * @param  array  $vars  Assoc array with the properties
     * @return object
     */
    protected function newInstance($class, array $vars)
    {
        $class = $this->filterClass($class);
        $object = new $class();
        $writable = get_class_vars($class);
        $setters = get_class_methods($class);
        $field_map = null;

        if ($object instanceof JsonDeserializable) {
            $field_map = $object->jsonDeserialize();
        }

        foreach ($vars as $property => $val) {
            $value4write = $val;
            $nestend_class = isset($this->objectmap[$property]) ? $this->objectmap[$property] : null;

            // Resolve nested object, before injection
            if (!is_null($nestend_class)) {
                $value4write = $this->resolveNestedClass($nestend_class, $value4write);
            }

            // Resolve field name
            if (!is_null($field_map) && isset($field_map[$property])) {
                $property = $field_map[$property];
            }

            // Inject value
            if (array_key_exists($property, $writable)) {
                // Property injection
                $object->$property = $value4write;
                continue;
            }

            // Setter method injection
            $setter = $this->setter_prefix . ucfirst($property) . $this->setter_suffix;
            if (in_array($setter, $setters)) {
                $object->$setter($value4write);
                continue;
            }

            // Last chance to inject
            $object->$property = $value4write;
        }

        return $object;
    }

    private function resolveNestedClass($nestend_class, $value4write)
    {
        if (is_object($value4write)) {
            if ($value4write instanceof JsonElementAbstract) {
                $value4write = $value4write->getRaw();
            }
            $value4write = $this->newInstance($nestend_class, get_object_vars($value4write));
        } elseif (is_array($value4write)) {
            $new_arr = [];
            foreach ($value4write as $v) {
                if (is_object($v)) {
                    $new_arr[] = $this->newInstance($nestend_class, get_object_vars($v));
                } else {
                    $new_arr[] = $v;
                }
            }
            $value4write = $new_arr;
        }
        return $value4write;
    }

    /**
     * @param  string $alias
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function filterAlias($alias)
    {
        if (!is_string($alias) || empty($alias)) {
            throw new InvalidArgumentException(
                'The "alias" parameter must be string type, but supplied "' .gettype($alias) . '"'
            );
        }
        return $alias;
    }

    /**
     * @param  string $class
     * @return string
     *
     * @throws InvalidArgumentException
     */
    private function filterClass($class)
    {
        if (!class_exists($class)) {
            throw new InvalidArgumentException("Class '$class' not found");
        }
        return $class;
    }

    /**
     * @param  array  $map
     * @param  string $setter_prefix
     * @param  string $setter_suffix
     * @return JsonObjectMapInterface
     */
    public static function create(array $map = [], $setter_prefix = 'set', $setter_suffix = '')
    {
        return new JsonObjectMap($map, $setter_prefix, $setter_suffix);
    }
}
