<?php
namespace d84\Phson\Document\Exception;

use d84\Phson\Document\Element\JsonPrimitive;

/**
 * UnexpectedPrimitiveException
 */
class UnexpectedPrimitiveException extends JsonElementException
{
    /**
     * @var JsonPrimitive
     */
    private $element;
    /**
     * @var string
     */
    private $expected_type;

    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "Expected type '%s', but supplied '%s'";

    /**
     * __construct
     *
     * @param JsonPrimitive $element
     * @param string        $expected_type
     */
    public function __construct(JsonPrimitive $element, $expected_type)
    {
        $this->expected_type = $expected_type;
        $this->element = $element;

        $message = sprintf(self::MESSAGE, $this->expected_type, gettype($element->getRaw()));

        parent::__construct($message);
    }

    /**
     * getElement
     *
     * @return JsonPrimitive
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * getExpectedType
     *
     * @return string
     */
    public function getExpectedType()
    {
        return $this->expected_type;
    }
}
