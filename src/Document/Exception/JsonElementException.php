<?php
namespace d84\Phson\Document\Exception;

/**
 * JsonElementException
 */
class JsonElementException extends JsonDocumentException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
