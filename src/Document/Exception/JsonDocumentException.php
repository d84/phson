<?php
namespace d84\Phson\Document\Exception;

use d84\Phson\Exception\JsonException;

/**
 * JsonDocumentException
 */
class JsonDocumentException extends JsonException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
