<?php
namespace d84\Phson\Document\Exception;

/**
 * UnexpectedElementException
 */
class UnexpectedElementException extends JsonElementException
{
    /**
     * @var mixed
     */
    private $element;
    /**
     * Class name of the expected json element.
     * In case of array - represents array of class names
     *
     * @var string|array
     */
    private $expected;

    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "Expected element '%s', but supplied '%s'";

    /**
     * __construct
     *
     * @param mixed        $supplied_element
     * @param string|array $expected_element
     */
    public function __construct($supplied_element, $expected_element)
    {
        $this->expected = $expected_element;
        $this->element = $supplied_element;

        $supp_str = '';
        if (is_object($this->element)) {
            $supp_str = get_class($this->element);
        } elseif (is_array($this->element)) {
            $supp_str = implode(',', $this->element);
        } else {
            $supp_str = strval($this->element);
        }

        $message = sprintf(self::MESSAGE, $this->getExpectedElementName(), $supp_str);

        parent::__construct($message);
    }

    /**
     * getElement
     *
     * @return mixed
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * getExpectedElementName
     *
     * @return string
     */
    public function getExpectedElementName()
    {
        if (is_array($this->expected)) {
            return implode(',', $this->expected);
        }
        return $this->expected;
    }
}
