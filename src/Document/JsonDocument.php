<?php
namespace d84\Phson\Document;

use InvalidArgumentException;
use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonArray;
use d84\Phson\Document\Exception\JsonElementException;
use d84\Phson\Document\Exception\JsonDocumentException;

/**
 * JsonDocument
 */
class JsonDocument
{
    const OPT_PRETTY_PRINT = 'prettyprint';

    /**
     * @var array
     */
    private $options;
    /**
     * @var JsonObjectMapInterface|null
     */
    private $objectmap;

    /**
     * @param JsonObjectMapInterface|null $objectmap
     */
    public function __construct(JsonObjectMapInterface $objectmap = null)
    {
        $this->objectmap = null;
        if (!is_null($objectmap)) {
            $this->setObjectMap($objectmap);
        }

        $this->options = [
          self::OPT_PRETTY_PRINT => false
        ];
    }

    /**
     * @return JsonDocument
     */
    public function enablePrettyPrint()
    {
        $this->options[self::OPT_PRETTY_PRINT] = true;
        return $this;
    }

    /**
     * @return JsonDocument
     */
    public function disablePrettyPrint()
    {
        $this->options[self::OPT_PRETTY_PRINT] = false;
        return $this;
    }

    /**
     * @param JsonObjectMapInterface $objectmap
     * @return JsonDocument
     */
    public function setObjectMap(JsonObjectMapInterface $objectmap)
    {
        $this->objectmap = $objectmap;
        return $this;
    }

    /**
     * @return JsonObjectMapInterface|null
     */
    public function getObjectMap()
    {
        return $this->objectmap;
    }

    /**
     * $class!=NULL and $json=Object => instance of $class
     * If $class is not null and $json represents an object,
     * will return instance of the $class with populated properties from the JSON document.
     *
     * $class!=NULL and $json=Array => [instance of $class, instance of $class,...]
     * If $class is not null and $json represents an array, will return the array of the $class instances.
     *
     * $class=NULL => JsonElementAbstract
     * If $class is null, will return the instance of JsonElementAbstract
     * with the type which conforms to the $json data value.
     *
     * The array $objectmap will be used in case the $class parameter is not empty.
     *
     * @param  string      $json
     * @param  string|null $class      The class name which will be created from json string
     * @param  array       $objectmap  Nested class objectmaps
     * @return mixed
     *
     * @throws JsonDocumentException
     * @throws JsonElementException
     * @throws InvalidArgumentException
     */
    public function fromJson($json, $class = null, array $objectmap = [])
    {
        $json_element = self::fromJsonString($json);

        if (is_null($class) || (!$json_element instanceof JsonObject && !$json_element instanceof JsonArray)) {
            return $json_element;
        }

        if (!class_exists($class)) {
            throw new InvalidArgumentException("Class '$class' not found");
        }

        $object_mapper = $this->getObjectMap();

        if (is_null($object_mapper) || !empty($objectmap)) {
            $object_mapper = JsonObjectMap::create($objectmap);
        }

        return $object_mapper->resolve($json_element, $class);
    }

    /**
     * @param  mixed $value
     * @param  array $options
     * @return string
     */
    public function toJson($value, array $options = [])
    {
        $common_opts = [];
        if (!empty($options)) {
            $common_opts = array_merge($this->options, $options);
        }
        return self::toJsonString($value, $this->buildOptions($common_opts));
    }

    /**
     * See http://php.net/manual/ru/json.constants.php
     *
     * @param array $options
     * @return int
     */
    private function buildOptions(array $options)
    {
        $out_options = 0;
        if (isset($options[self::OPT_PRETTY_PRINT]) && $options[self::OPT_PRETTY_PRINT] === true) {
            $out_options |= JSON_PRETTY_PRINT;
        }
        return $out_options;
    }

    /**
     * @param  string $json
     * @return JsonElementAbstract
     *
     * @throws InvalidArgumentException
     * @throws JsonDocumentException
     */
    public static function fromJsonString($json)
    {
        if (!is_string($json)) {
            $type = gettype($json);
            $msg = "The 'json' parameter must be string type, but supplied '$type'";
            throw new InvalidArgumentException($msg);
        }
        $json_element = @json_decode($json);
        if ($json_element === null && self::getLastError() !== JSON_ERROR_NONE) {
            throw new JsonDocumentException("Unable to parse JSON. " . self::getLastErrorMsg());
        }
        return JsonElementAbstract::wrap($json_element);
    }

    /**
     * @param  mixed $value
     * @param  int   $options
     * @return string
     */
    public static function toJsonString($value, $options = 0)
    {
        if (is_object($value) && $value instanceof JsonElementAbstract) {
            $value = $value->getRaw();
        }
        return json_encode($value, $options);
    }

    /**
     * @param  mixed $value
     * @return JsonElementAbstract
     *
     * @throws JsonElementException
     */
    public static function element($value)
    {
        return JsonElementAbstract::wrap($value);
    }

    /**
     * See http://php.net/manual/ru/function.json-last-error.php
     *
     * @return int
     */
    public static function getLastError()
    {
        return json_last_error();
    }

    /**
     * See http://php.net/manual/ru/function.json-last-error-msg.php
     *
     * @return string
     */
    public static function getLastErrorMsg()
    {
        return json_last_error_msg();
    }
}
