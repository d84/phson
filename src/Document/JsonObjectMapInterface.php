<?php
namespace d84\Phson\Document;

use d84\Phson\Document\Element\JsonElementAbstract;

/**
 * JsonObjectMapInterface
 */
interface JsonObjectMapInterface
{
    /**
     * @param  JsonElementAbstract $element
     * @param  string              $class
     * @return object
     */
    public function resolve(JsonElementAbstract $element, $class);
}
