<?php
namespace d84\Phson\Document;

/**
 * JsonDeserializable
 */
interface JsonDeserializable
{
    /**
     * @return array
     */
    public function jsonDeserialize();
}
