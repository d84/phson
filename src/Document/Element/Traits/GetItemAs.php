<?php
namespace d84\Phson\Document\Element\Traits;

use Carbon\Carbon;
use d84\Phson\Document\Exception\JsonElementException;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Document\Element\JsonArray;

/**
 * GetItemAs
 */
trait GetItemAs
{
    /**
     * @param  mixed $index
     * @param  mixed $default
     * @return int
     *
     * @throws JsonElementException
     */
    public function asInteger($index, $default = null)
    {
        if (!is_int($default) && !is_null($default)) {
            $supplied = gettype($default);
            $msg = "Expected 'int' type for the default value, but supplied '$supplied'";
            throw new JsonElementException($msg);
        }
        $v = $this->get($index, true);
        if (is_null($v)) {
            return $default;
        }
        if ($v instanceof JsonPrimitive) {
            return $v->asInteger();
        }
        return $default;
    }

    /**
     * @param  mixed $index
     * @param  mixed $default
     * @return float
     *
     * @throws JsonElementException
     */
    public function asFloat($index, $default = null)
    {
        if (!is_float($default) && !is_null($default)) {
            $supplied = gettype($default);
            throw new JsonElementException("Expected 'float' type for the default value, but supplied '$supplied'");
        }
        $v = $this->get($index, true);
        if (is_null($v)) {
            return $default;
        }
        if ($v instanceof JsonPrimitive) {
            return $v->asFloat();
        }
        return $default;
    }

    /**
     * @param  mixed $index
     * @param  mixed $default
     * @return int|float
     *
     * @throws JsonElementException
     */
    public function asNumber($index, $default = null)
    {
        if (!is_float($default) && !is_int($default) && !is_null($default)) {
            $supplied = gettype($default);
            $msg = "Expected 'float' or 'int' type for the default value, but supplied '$supplied'";
            throw new JsonElementException($msg);
        }
        $v = $this->get($index, true);
        if (is_null($v)) {
            return $default;
        }
        if ($v instanceof JsonPrimitive) {
            return $v->asNumber();
        }
        return $default;
    }

    /**
     * @param  mixed $index
     * @param  mixed $default
     * @return string
     *
     * @throws JsonElementException
     */
    public function asString($index, $default = null)
    {
        if (!is_string($default) && !is_null($default)) {
            $supplied = gettype($default);
            throw new JsonElementException("Expected 'string' type for the default value, but supplied '$supplied'");
        }
        $v = $this->get($index, true);
        if (is_null($v)) {
            return $default;
        }
        if ($v instanceof JsonPrimitive) {
            return $v->asString();
        }
        if ($v instanceof JsonArray) {
            return implode(',', $v->getAll(false));
        }
        return $default;
    }

    /**
     * @param  mixed $index
     * @param  mixed $default
     * @return bool
     *
     * @throws JsonElementException
     */
    public function asBoolean($index, $default = null)
    {
        if (!is_bool($default) && !is_null($default)) {
            $supplied = gettype($default);
            throw new JsonElementException("Expected 'bool' type for the default value, but supplied '$supplied'");
        }
        $v = $this->get($index, true);
        if (is_null($v)) {
            return $default;
        }
        if ($v instanceof JsonPrimitive) {
            return $v->asBoolean();
        }
        return $default;
    }

    /**
     * Returns instance of Carbon class for an item which is contained at $index offset.
     * In case when the item is not an instance of JsonPrimitive(string/int) class, it returns FALSE.
     *
     * @param  mixed $index
     * @param  mixed $format
     * @param  mixed $tz
     * @return Carbon|false
     */
    public function asDate($index, $format = null, $tz = null)
    {
        $v = $this->get($index, true);
        if (!is_null($v) && $v instanceof JsonPrimitive && ($v->isString() || $v->isInteger())) {
            return $v->asDate($format, $tz);
        }
        return false;
    }
}
