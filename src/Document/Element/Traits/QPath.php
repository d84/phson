<?php
namespace d84\Phson\Document\Element\Traits;

use InvalidArgumentException;
use d84\Phson\Document\Element\JsonElementAbstract;

/**
 * QPath
 */
trait QPath
{
    /**
     * @param  object|array $value
     * @param  string       $path
     * @param  int          $max_path_length
     * @return JsonElementAbstract|false
     */
    protected function query($value, $path, $max_path_length = 1024)
    {
        $path = $this->filterPath($path);
        if ($path === '/') {
            return $value;
        }
        $path = $this->preparePath($path, $max_path_length);
        return $this->findPath($value, $path);
    }

    /**
     * @param  string $path
     * @return string
     *
     * @throws InvalidArgumentException
     */
    protected function filterPath($path)
    {
        if (!is_string($path)) {
            throw new InvalidArgumentException(
                "The 'path' parameter must be string type, but supplied " . gettype($path)
            );
        }
        if (empty($path)) {
            throw new InvalidArgumentException(
                "The 'path' parameter is empty"
            );
        }
        return $path;
    }

    /**
     * @param   string $path
     * @param   int    $max_path_length
     * @return array
     */
    protected function preparePath($path, $max_path_length)
    {
        if ($path[0] === '/') {
            $path = ltrim($path, '/');
        }
        return explode('/', $path, $max_path_length);
    }

    /**
     * @param  object $node
     * @param  array  $path
     * @return JsonElementAbstract|false
     */
    protected function findPath($node, array $path)
    {
        if (!is_object($node)) {
            return false;
        }

        $next_node = null;
        $prop_key = array_shift($path);

        if (is_null($prop_key)) {
            return false;
        }

        if (property_exists($node, $prop_key)) {
            $next_node = $node->$prop_key;
        } else {
            $getter = 'get'.ucfirst($prop_key);
            if (is_callable(array($node, $getter))) {
                $next_node = $node->$getter();
            }
        }

        if (is_null($next_node)) {
            return false;
        }

        if (empty($path)) {
            return $next_node;
        }

        return $this->findPath($next_node, $path);
    }
}
