<?php
namespace d84\Phson\Document\Element;

use ArrayIterator;
use ArrayAccess;
use stdClass;
use InvalidArgumentException;
use Carbon\Carbon;

use d84\Phson\Document\Element\JsonArray;
use d84\Phson\Document\Element\Traits\GetItemAs;
use d84\Phson\Document\Element\Traits\QPath;
use d84\Phson\Document\Exception\UnexpectedElementException;

/**
 * JsonObject
 */
class JsonObject extends JsonElementAbstract implements ArrayAccess
{
    // asInteger(), asString(), asFloat()...
    use GetItemAs;
    // find()
    use QPath;

    /**
     * MAX_PATH_LENGTH Used by the QPath
     *
     * @var int
     */
    const MAX_PATH_LENGTH = 1024;

    /**
     * @param object|array|null $value
     */
    public function __construct($value = null)
    {
        if (is_array($value) && JsonArray::isAssoc($value)) {
            $value = self::fromArray($value, false);
        }
        parent::__construct($value);
        $this->selfcheck();
    }

    /**
     * @throws UnexpectedElementException
     */
    private function selfcheck()
    {
        if ($this->isNull()) {
            $this->value = new stdClass();
        }
        if (!$this->isObject()) {
            throw new UnexpectedElementException($this->value, JsonObject::class);
        }
    }

    /**
     * @param  string $path
     * @return JsonElementAbstract|false
     */
    public function find($path)
    {
        $result = $this->query($this->value, $path, self::MAX_PATH_LENGTH);
        if ($result === false) {
            return false;
        }
        return self::wrap($result);
    }

    /**
     * @return int
     */
    public function getPropertiesCount()
    {
        return count(get_object_vars($this->value));
    }

    /**
     * @return array
     */
    public function getPropertyKeys()
    {
        return array_keys(get_object_vars($this->value));
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->getPropertiesCount() === 0;
    }

    /**
     * @param string              $property
     * @param JsonElementAbstract $element
     */
    public function set($property, JsonElementAbstract $element)
    {
        $property = $this->filterProperty($property);
        $value = $element->getRaw();

        if ($this->hasSetter($property)) {
            $setter = $this->getSetterName($property);
            $this->value->$setter($value);
        } else {
            $this->value->$property = $value;
        }

        return $this;
    }

    /**
     * @param string $property
     * @param mixed  $value
     */
    public function setProperty($property, $value)
    {
        $property = $this->filterProperty($property);

        if ($value instanceof JsonElementAbstract) {
            $value = $value->getRaw();
        }

        // Carbon\Carbon: Autocast date to string
        if ($value instanceof Carbon) {
            $value = $value->toDateTimeString();
        }

        if ($this->hasSetter($property)) {
            $setter = $this->getSetterName($property);
            $this->value->$setter($value);
        } else {
            $this->value->$property = $value;
        }

        return $this;
    }

    /**
     * @param  string $property
     * @return JsonElementAbstract|false
     */
    public function remove($property)
    {
        if ($this->has($property) && $this->isPropertyAccessible($property)) {
            $old_value = self::wrap($this->value->$property);
            unset($this->value->$property);
            return $old_value;
        }
        return false;
    }

    /**
     * @param  string $property
     * @return bool
     */
    public function has($property)
    {
        return property_exists($this->value, $this->filterProperty($property));
    }

    /**
     * @param  array   $properties
     * @return array   An array of unknown properties (that doesn't exist in the object)
     */
    public function checkProperties(array $properties)
    {
        $keys = $this->getPropertyKeys();
        return array_diff($properties, $keys);
    }

    /**
     * @param  string $property
     * @param  bool   $wrap
     * @param  mixed  $default
     * @return object|null
     */
    public function get($property, $wrap = true, $default = null)
    {
        $result = $default;
        if ($this->has($property)) {
            if ($this->isPropertyAccessible($property)) {
                // Object has readable (public) property
                $result = $this->value->$property;
            } elseif ($this->hasGetter($property)) {
                $getter = $this->getGetterName($property);
                $result = $this->value->$getter();
            }
        }
        if (!is_null($result) && $wrap === true) {
            $result = self::wrap($result);
        }
        return $result;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->value);
    }

    /**
     * @param  bool $wrap
     * @return array
     */
    public function entrySet($wrap = true)
    {
        if ($wrap === true) {
            $properties = get_object_vars($this->value);
            foreach ($properties as $key => $value) {
                $properties[$key] = self::wrap($value);
            }
            return $properties;
        }
        return get_object_vars($this->value);
    }

    /**
     * @param  string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * @param  string $offset
     * @return object|null $offset
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @param string $offset
     * @param mixed  $value
     */
    public function offsetSet($offset, $value)
    {
        $this->setProperty($offset, $value);
    }

    /**
     * @param string $offset
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }

    /**
     * @throws InvalidArgumentException
     *
     * @param  string $property
     * @return string
     */
    private function filterProperty($property)
    {
        if (empty($property) || !is_string($property)) {
            throw new InvalidArgumentException("The 'property' argument must be 'string' type with non empty value");
        }
        return $property;
    }

    /**
     * @param  string $property
     * @return bool
     */
    private function isPropertyAccessible($property)
    {
        if ($this->has($property)) {
            return array_key_exists($property, get_object_vars($this->value));
        }
        return false;
    }

    /**
     * @param  string $property
     * @return bool
     */
    private function hasGetter($property)
    {
        return is_callable(array($this->value, $this->getGetterName($property)));
        //method_exists($this->value, $this->getGetterName($property));
    }

    /**
     * @param  string $property
     * @return bool
     */
    private function hasSetter($property)
    {
        return is_callable(array($this->value, $this->getSetterName($property)));
    }

    /**
     * @param  string $property
     * @return string
     */
    private function getGetterName($property)
    {
        return 'get' . ucfirst($property);
    }

    /**
     * @param  string $property
     * @return string
     */
    private function getSetterName($property)
    {
        return 'set' . ucfirst($property);
    }

    /**
     * @param  JsonElementAbstract $e
     * @return bool
     */
    public function equals(JsonElementAbstract $e)
    {
        if (!$e->isObject()) {
            return false;
        }
        return JsonElementAbstract::compare($this, $e);
    }

    /**
     * Creates instance of the JsonObject class from given assoc array
     *
     * @param  array $array Assoc array
     * @param  bool  $wrap
     * @return JsonObject|object
     */
    public static function fromArray(array $array, $wrap = true)
    {
        $obj = new stdClass();
        foreach ($array as $key => $val) {
            if (JsonArray::isAssoc($val)) {
                $obj->$key = self::fromArray($val, false);
            } else {
                $obj->$key = $val;
            }
        }
        if ($wrap === true) {
            return new JsonObject($obj);
        }
        return $obj;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)json_encode($this->value);
    }
}
