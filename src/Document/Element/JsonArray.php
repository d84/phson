<?php
namespace d84\Phson\Document\Element;

use ArrayIterator;
use ArrayAccess;

use Carbon\Carbon;
use d84\Phson\Document\Element\Traits\GetItemAs;
use d84\Phson\Document\Exception\UnexpectedElementException;
use d84\Phson\Document\Exception\JsonElementException;

/**
 * JsonArray
 */
class JsonArray extends JsonElementAbstract implements ArrayAccess
{
    use GetItemAs;

    /**
     * @var int
     */
    private $size;

    /**
     * @throws UnexpectedElementException
     *
     * @param array $value
     */
    public function __construct($value = [])
    {
        parent::__construct($value);
        if (!$this->isArray()) {
            throw new UnexpectedElementException($value, JsonArray::class);
        }
        $this->size = count($this->value);
        $this->resolveElements();
    }

    /**
     * @param mixed $value
     */
    public function add($value)
    {
        if (self::isPrimitiveValue($value) || self::isArrayValue($value) || self::isNullValue($value)) {
            array_push($this->value, $value);
        } elseif (self::isObjectValue($value)) {
            // Carbon\Carbon: Autocast date to string
            if ($value instanceof Carbon) {
                $value = $value->toDateTimeString();
            } elseif ($value instanceof JsonElementAbstract) {
                $value = $value->getRaw();
            }
            array_push($this->value, $value);
        }
        $this->size++;
        return $this;
    }

    /**
     * @param JsonArray $array
     */
    public function addAll(JsonArray $array)
    {
        $this->value = array_merge($this->value, $array->getRaw());
        $this->size = count($this->value);
        return $this;
    }

    /**
     * @param JsonArray $array
     */
    public function setAll(JsonArray $array)
    {
        $this->value = $array->getRaw();
        $this->size = count($this->value);
        return $this;
    }

    /**
     * @throws JsonElementException
     *
     * @param  int   $index
     * @param  bool  $wrap
     * @param  mixed $default
     * @return mixed
     */
    public function get($index, $wrap = true, $default = null)
    {
        if (!is_integer($index)) {
            throw new JsonElementException(
                "The 'index' parameter must be 'integer' type, but supplied '" . gettype($index) . "'"
            );
        }
        if ($index < 0 || $index > $this->size()) {
            return  $default;
        }
        return ($wrap === true) ? self::wrap($this->value[$index]) : $this->value[$index];
    }

    /**
     * @param  bool $wrap
     * @return array
     */
    public function getAll($wrap = true)
    {
        if ($wrap === false) {
            return $this->value;
        }
        $result = [];
        $len = $this->size();
        for ($i = 0; $i < $len; $i++) {
            $result[$i] = self::wrap($this->value[$i]);
        }
        return $result;
    }

    /**
     * @param  bool $wrap
     * @return mixed
     */
    public function last($wrap = true)
    {
        if ($this->size() === 0) {
            return null;
        }
        $result = $this->value[$this->size() - 1];
        if ($wrap === true) {
            $result = JsonElementAbstract::wrap($result);
        }
        return $result;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->value);
    }

    /**
     * @throws JsonElementException
     *
     * @param  int $index
     * @return bool
     */
    public function has($index)
    {
        if (!is_integer($index)) {
            throw new JsonElementException(
                "The 'index' parameter must be 'integer' type, but supplied '" . gettype($index) . "'"
            );
        }
        return array_key_exists($index, $this->value);
    }

    /**
     * @throws JsonElementException
     *
     * @param  int   $index
     * @param  mixed $element
     * @return mixed|false
     */
    public function set($index, $element)
    {
        if ($this->has($index)) {
            $old_value = self::wrap($this->value[$index]);
            // Carbon\Carbon: Autocast date to string
            if ($element instanceof Carbon) {
                $element = $element->toDateTimeString();
            } elseif ($element instanceof JsonElementAbstract) {
                $element = $element->getRaw();
            }
            $this->value[$index] = $element;
            return $old_value;
        }
        return false;
    }

    /**
     * @param int $offset
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->value);//$this->has($offset);
    }

    /**
     * @param int $offset
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @param int   $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * @param int $offset
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }

    /**
     * @return int
     */
    public function size()
    {
        return $this->size;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->size === 0;
    }

    /**
     * @param  JsonElementAbstract $array
     * @return bool
     */
    public function equals(JsonElementAbstract $array)
    {
        if (!$array instanceof JsonArray) {
            return false;
        }

        if ($this->size() !== $array->size()) {
            return false;
        }

        return JsonElementAbstract::compare($this, $array);
    }

    /**
     * @param  JsonElementAbstract $element
     * @return bool
     */
    public function contains(JsonElementAbstract $element)
    {
        if ($element->isObject()) {
            return $this->containsObject($element);
        }
        if ($element->isArray()) {
            return $this->containsSubarray($element);
        }
        if ($element->isPrimitive()) {
            return $this->containsPrimitive($element, $this->value);
        }
        if ($element->isNull()) {
            return $this->containsNull($element, $this->value);
        }
    }

    /**
     * @param  JsonArray $array
     * @return JsonArray
     */
    public function merge(JsonArray $array)
    {
        return new JsonArray(array_merge($this->value, $array->getRaw()));
    }

    /**
     * Filters array by user function.
     * Creates new JsonArray object with filter result.
     *
     * @param  callable $filter_function Takes two arguments:
     *                                   1: Value
     *                                   2: Index
     * @return JsonArray
     */
    public function filter(callable $filter_function)
    {
        $result = [];
        foreach ($this->value as $ki => $v) {
            if ($filter_function($v, $ki) === true) {
                $result[] = $v;
            }
        }
        return new JsonArray($result);
    }

    /**
     * @param callable $user_function
     */
    public function each(callable $user_function)
    {
        foreach ($this->value as $ki => $v) {
            $user_function($v, $ki);
        }
    }

    /**
     * @param  int $index
     * @return mixed
     */
    public function remove($index)
    {
        if ($this->has($index)) {
            $old_value = self::wrap($this->value[$index]);
            unset($this->value[$index]);
            $this->size--;
            return $old_value;
        }
        return false;
    }

    /**
     * @return void
     */
    private function resolveElements()
    {
        $len = $this->size();
        for ($i = 0; $i < $len; $i++) {
            if (self::isObjectValue($this->value[$i]) && $this->value[$i] instanceof JsonElementAbstract) {
                $this->value[$i] = $this->value[$i]->getRaw();
            }
        }
    }

    /**
     * @param  JsonElementAbstract $element
     * @return bool
     */
    private function containsObject(JsonElementAbstract $element)
    {
        if (!$element instanceof JsonObject) {
            return false;
        }
        $element_inner_obj = $element->getRaw();
        $iterator = $this->getIterator();
        foreach ($iterator as $item) {
            if (is_object($item) && $item == $element_inner_obj) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param  JsonElementAbstract $subarray
     * @return bool
     */
    private function containsSubarray(JsonElementAbstract $subarray)
    {
        if (!$subarray instanceof JsonArray) {
            return false;
        }

        $length = $this->size();
        $sub_length = $subarray->size();

        if ($length < $sub_length) {
            return false;
        }

        if ($length === $sub_length) {
            return $this->equals($subarray);
        }

        for ($i = 0; $i < $length; $i++) {
            if (($length - $i) < $sub_length) {
                return false;
            }
            $out = $this[$i];
            if (JsonElementAbstract::compare($out, $subarray[0])) {
                $temp_i = $i + 1;
                $flag = true;
                for ($j = 1; $j < $sub_length; $j++) {
                    $in = $subarray[$j];
                    $temp_out = $this[$temp_i];
                    if (!JsonElementAbstract::compare($temp_out, $in)) {
                        $flag = false;
                        break;
                    }
                    $temp_i++;
                }
                if ($flag) {
                    break;
                }
            }
        }

        return true;
    }

    /**
     * @param  JsonElementAbstract $element
     * @param  array               $array
     * @return bool
     */
    private function containsPrimitive(JsonElementAbstract $element, array $array)
    {
        if (!$element instanceof JsonPrimitive) {
            return false;
        }
        return in_array($element->getRaw(), $array);
    }

    /**
     * @param  JsonElementAbstract $element
     * @param  array               $array
     * @return bool
     */
    private function containsNull(JsonElementAbstract $element, array $array)
    {
        if (!$element instanceof JsonNull) {
            return false;
        }
        return in_array($element->getRaw(), $array);
    }

    /**
     * @param  mixed $value
     * @return bool
     */
    public static function isAssoc($value)
    {
        if (!is_array($value) || [] === $value) {
            return false;
        }
        return array_keys($value) !== range(0, count($value) - 1);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)json_encode($this->value);
    }
}
