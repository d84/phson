<?php
namespace d84\Phson\Document\Element;

/**
 * JsonNull
 */
class JsonNull extends JsonElementAbstract
{
    /**
     */
    public function __construct()
    {
        parent::__construct(null);
    }

    /**
     * @param  JsonElementAbstract $e
     * @return bool
     */
    public function equals(JsonElementAbstract $e)
    {
        if (!$e->isNull()) {
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'null';
    }
}
