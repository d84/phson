<?php
namespace d84\Phson\Document\Element;

use d84\Phson\Document\Exception\JsonElementException;

/**
 * JsonElementAbstract
 */
abstract class JsonElementAbstract
{
    /**
     * The original value that was wrapped
     *
     * @var mixed
     */
    protected $value;

    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     */
    abstract public function __toString();

    /**
     * @param  JsonElementAbstract $e
     * @return bool
     */
    abstract public function equals(JsonElementAbstract $e);

    /**
     * Returns wrapped value
     *
     * @return mixed
     */
    public function getRaw()
    {
        return $this->value;
    }

    /**
     * Returns TRUE if wrapped value is an object, otherwise returns FALSE
     *
     * @return bool
     */
    public function isObject()
    {
        return self::isObjectValue($this->value);
    }

    /**
     * Returns TRUE if wrapped value is an array, otherwise returns FALSE
     *
     * @return bool
     */
    public function isArray()
    {
        return self::isArrayValue($this->value);
    }

    /**
     * Returns TRUE if wrapped value is a primitive(int,float,string,bool), otherwise returns FALSE
     *
     * @return bool
     */
    public function isPrimitive()
    {
        return self::isPrimitiveValue($this->value);
    }

    /**
     * Returns TRUE if wrapped value is null, otherwise returns FALSE
     *
     * @return bool
     */
    public function isNull()
    {
        return self::isNullValue($this->value);
    }

    /**
     * Returns element type
     *
     * @return string|null
     */
    public function getType()
    {
        if (self::isNullValue($this->value)) {
            return 'null';
        }
        if (self::isObjectValue($this->value)) {
            return 'object';
        }
        if (self::isArrayValue($this->value)) {
            return 'array';
        }
        if (is_string($this->value)) {
            return 'string';
        }
        if (is_int($this->value) || is_float($this->value)) {
            return 'number';
        }
        if (is_bool($this->value)) {
            return 'boolean';
        }
        return null;
    }

    /**
     * Returns wrapped value into corresponding JsonElement
     *
     * @param  mixed $value
     * @return JsonObject|JsonArray|JsonPrimitive|JsonNull
     *
     * @throws JsonElementException
     */
    public static function wrap($value)
    {
        if ($value instanceof JsonElementAbstract) {
            return $value;
        }
        if (self::isNullValue($value)) {
            return new JsonNull();
        }
        if (self::isObjectValue($value)) {
            return new JsonObject($value);
        }
        if (self::isPrimitiveValue($value)) {
            return new JsonPrimitive($value);
        }
        if (self::isArrayValue($value)) {
            return new JsonArray($value);
        }
        throw new JsonElementException("Unable to wrap '" . gettype($value) . "'.");
    }

    /**
     * Returns TRUE if value belongs to the primitive type (int,float,string,bool), otherwise returns FALSE
     *
     * @param  mixed $value
     * @return bool
     */
    public static function isPrimitiveValue($value)
    {
        return (is_float($value) || is_string($value) || is_int($value) || is_bool($value));
    }

    /**
     * Returns TRUE if value belongs to the NULL type, otherwise returns FALSE
     *
     * @param  mixed $value
     * @return bool
     */
    public static function isNullValue($value)
    {
        return is_null($value);
    }

    /**
     * Returns TRUE if value belongs to the object type, otherwise returns FALSE
     *
     * @param  mixed $value
     * @return bool
     */
    public static function isObjectValue($value)
    {
        return is_object($value);
    }

    /**
     * Returns TRUE if value belongs to the array type, otherwise returns FALSE
     *
     * @param  mixed $value
     * @return bool
     */
    public static function isArrayValue($value)
    {
        return is_array($value);
    }

    /**
     * Returns TRUE if value belongs to the supported types, otherwise return FALSE
     *
     * @param  mixed $value
     * @return bool
     */
    public static function isSupportedValue($value)
    {
        return (
          self::isPrimitiveValue($value) ||
          self::isNullValue($value) ||
          self::isArrayValue($value) ||
          self::isObjectValue($value)
        );
    }

    /**
     * Compares two elements
     *
     * @param  mixed $e1
     * @param  mixed $e2
     * @return bool
     */
    public static function compare($e1, $e2)
    {
        if ($e1 instanceof JsonElementAbstract) {
            $e1 = $e1->getRaw();
        }
        if ($e2 instanceof JsonElementAbstract) {
            $e2 = $e2->getRaw();
        }

        $te1 = gettype($e1);
        $te2 = gettype($e2);
        if ($te1 !== $te2) {
            return false;
        }

        $result = false;
        switch ($te1) {
            case 'boolean':
            case 'integer':
            case 'double':
            case 'float':
            case 'string':
            case 'NULL':
                $result = ($e1 === $e2) ? true : false;
                break;
            case 'object':
                $result = (get_class($e1) === get_class($e2) && $e1 == $e2) ? true : false;
                break;
            case 'array':
                $diff = array_udiff(
                    $e1,
                    $e2,
                    function ($a, $b) {
                        return JsonElementAbstract::compare($a, $b) ? 0 : 1;
                    }
                );
                $result = (count($diff) === 0);
                break;
        }

        return $result;
    }
}
