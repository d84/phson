<?php
namespace d84\Phson\Document\Element;

use InvalidArgumentException;
use d84\Phson\Document\Exception\UnexpectedElementException;
use Carbon\Carbon;

/**
 * JsonPrimitive
 */
class JsonPrimitive extends JsonElementAbstract
{
    /**
     * @throws UnexpectedElementException
     *
     * @param mixed $value Primitive value
     */
    public function __construct($value)
    {
        parent::__construct($value);
        if (!$this->isPrimitive()) {
            throw new UnexpectedElementException($value, JsonPrimitive::class);
        }
    }

    /**
     * Setes new value for the primitive
     *
     * @param mixed $value Primitive type (string/number/bool)
     */
    public function set($value)
    {
        if (!self::isPrimitiveValue($value)) {
            throw new InvalidArgumentException("Value must be primitive, but supplied '" . gettype($value) . "'");
        }
        $this->value = $value;
        return $this;
    }

    /**
     * Returns TRUE if wrapped value is integer
     *
     * @return bool
     */
    public function isInteger()
    {
        return is_int($this->value);
    }

    /**
     * Return wrapped value as integer
     *
     * @return int|null
     */
    public function asInteger()
    {
        if ($this->isString()) {
            $int = filter_var($this->value, FILTER_VALIDATE_INT|FILTER_VALIDATE_FLOAT);
            if ($int === false) {
                return null;
            }
            return intval($int);
        }
        return intval($this->value);
    }

    /**
     * Returns TRUE if wrapped value is float
     *
     * @return bool
     */
    public function isFloat()
    {
        return is_float($this->value);
    }

    /**
     * Return wrapped value as integer
     *
     * @return float|null
     */
    public function asFloat()
    {
        if ($this->isString()) {
            $float = filter_var($this->value, FILTER_VALIDATE_INT|FILTER_VALIDATE_FLOAT);
            if ($float === false) {
                return null;
            }
            return floatval($float);
        }
        return floatval($this->value);
    }

    /**
     * @return int|float|null
     */
    public function asNumber()
    {
        if ($this->isString()) {
            $num = false;
            if (strpos($this->value, '.') !== false) {
                $num = filter_var($this->value, FILTER_VALIDATE_FLOAT);
            } else {
                $num = filter_var($this->value, FILTER_VALIDATE_INT);
            }
            if ($num === false) {
                return null;
            }
            return $num;
        }
        if ($this->isInteger() || $this->isFloat()) {
            return $this->value;
        }
        return intval($this->value);
    }

    /**
     * @return bool
     */
    public function isNumber()
    {
        return $this->isInteger() || $this->isFloat();
    }

    /**
     * Returns TRUE if wrapped value is string
     *
     * @return bool
     */
    public function isString()
    {
        return is_string($this->value);
    }

    /**
     * Return wrapped value as string
     *
     * @return string
     */
    public function asString()
    {
        if ($this->isBoolean()) {
            return ($this->value) ? 'true' : 'false';
        }
        return strval($this->value);
    }

    /**
     * Returns TRUE if wrapped value is bool
     *
     * @return bool
     */
    public function isBoolean()
    {
        return is_bool($this->value);
    }

    /**
     * Return wrapped value as bool
     *
     * @return bool|null
     */
    public function asBoolean()
    {
        if ($this->isString()) {
            return filter_var($this->value, FILTER_VALIDATE_BOOLEAN);
        }
        return boolval($this->value);
    }

    /**
     * @param  string $format
     * @return bool
     */
    public function isDate($format = 'd.m.Y H:i:s')
    {
        $isdate = false;
        try {
            if ($this->isInteger()) {
                $isdate = Carbon::createFromTimestamp($this->value);
            }
            if ($this->isString()) {
                $isdate = Carbon::createFromFormat($format, $this->value);
            }
        } catch (InvalidArgumentException $e) {
            return false;
        }
        if ($isdate instanceof Carbon) {
            return true;
        }
        return false;
    }

    /**
     * Returns Carbon date object that represents value of primitive.
     * Supports string/int conversion to the Carbon class, for others types return FALSE.
     *
     * @param  string $format
     * @param  string $tz
     * @return Carbon|false
     */
    public function asDate($format = null, $tz = null)
    {
        if ($this->isString()) {
            if (is_null($format)) {
                return Carbon::parse($this->value);
            }
            return Carbon::createFromFormat($format, $this->value, $tz);
        }
        if ($this->isInteger()) {
            return Carbon::createFromTimestamp($this->value, $tz);
        }
        return false;
    }

    /**
     * @param  JsonElementAbstract $e
     * @return bool
     */
    public function equals(JsonElementAbstract $e)
    {
        if (!$e->isPrimitive()) {
            return false;
        }
        return JsonElementAbstract::compare($this, $e);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->asString();
    }
}
