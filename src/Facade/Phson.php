<?php
namespace d84\Phson\Facade;

use d84\Phson\Document\JsonDocument;
use d84\Phson\Document\JsonObjectMap;
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Document\Exception\JsonDocumentException;
use d84\Phson\Schema\Validator\Validator;

/**
 * Pson facade
 */
abstract class Phson
{
    /**
     * @var JsonDocument
     */
    private static $json_document = null;

    /**
     * @var Validator
     */
    private static $json_validator = null;

    /**
     * @return JsonDocument
     */
    public static function getDocument()
    {
        if (self::$json_document === null) {
            self::$json_document = new JsonDocument();
        }
        return self::$json_document;
    }

    /**
     * @return Validator
     */
    public static function getValidator()
    {
        if (self::$json_validator === null) {
            self::$json_validator = new Validator();
        }
        return self::$json_validator;
    }

    /**
     * Convert json string to an object.
     * If $class is not null, will create instance of $class.
     * Otherwise will create instance of the JsonElementAbstract class.
     * Array of nested object from the $objectmap parameter will be used in case $class is not null.
     *
     * @param  string      $json
     * @param  string|null $class
     * @param  array       $objectmap Nested object map for $class
     * @return JsonElementAbstract|object
     */
    public static function fromJson($json, $class = null, array $objectmap = [])
    {
        return static::getDocument()->fromJson($json, $class, $objectmap);
    }

    /**
     * @param  mixed $value
     * @param  array $options
     * @return string
     */
    public static function toJson($value, array $options = [])
    {
        return static::getDocument()->toJson($value, $options);
    }

    /**
     * Validates json document by means given json schema
     *
     * @param  string|object $document
     * @param  string|object $schema
     * @param  string|null $class
     * @param  array       $objectmap Nested object map for $class
     * @return JsonElementAbstract|object
     */
    public static function validate($document, $schema, $class = null, array $objectmap = [])
    {
        $element = self::getValidator()->validate($document, $schema);
        if (!is_null($class)) {
            return JsonObjectMap::create($objectmap)->resolve($element, $class);
        }
        return $element;
    }

    /**
     * Creates instance of the JsonObject class from given assoc array
     *
     * @param  array $array Assoc array
     * @return JsonObject
     *
     * @throws JsonDocumentException
     */
    public static function fromArray(array $array)
    {
        $new_obj = JsonObject::fromArray($array);
        if (!$new_obj instanceof JsonObject) {
            throw new JsonDocumentException("Unable to create JsonObject from array (" . implode(',', $array) . ")");
        }
        return $new_obj;
    }
}
