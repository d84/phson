<?php
namespace d84\Phson\Schema;

use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Schema\Exception\JsonSchemaException;

/**
 * ObjectSchema
 */
class ObjectSchema extends SchemaAbstract
{
    public function __construct(JsonObject $schema, SchemaAbstract $parent = null)
    {
        parent::__construct($schema, $parent);
    }

    /**
     * @return int|null
     */
    public function getMinProperties()
    {
        /** @var JsonPrimitive|null $min_properties */
        $min_properties = $this->getKeyword('minProperties', ['number']);

        if (is_null($min_properties)) {
            return null;
        }

        $min_properties = $min_properties->asInteger();
        if ($min_properties < 0) {
            $msg = "Keyword 'minProperties' must be a non-negative integer, but supplied '$min_properties'";
            throw new JsonSchemaException($msg);
        }

        return $min_properties;
    }

    /**
     * @return int|null
     */
    public function getMaxProperties()
    {
        /** @var JsonPrimitive|null $max_properties */
        $max_properties = $this->getKeyword('maxProperties', ['number']);

        if (is_null($max_properties)) {
            return null;
        }

        $max_properties = $max_properties->asInteger();
        if ($max_properties < 0) {
            $msg = "Keyword 'maxProperties' must be a non-negative integer, but supplied '$max_properties'";
            throw new JsonSchemaException($msg);
        }

        return $max_properties;
    }

    /**
     * @return array
     */
    public function getRequired()
    {
        return $this->getNVL('required', ['array'], []);
    }

    /**
     * @return JsonObject|null
     */
    public function getProperties()
    {
        /** @var JsonObject|null $properties */
        $properties = $this->getKeyword('properties', ['object']);
        return $properties;
    }

    /**
     * @return JsonObject|null
     */
    public function getPatternProperties()
    {
        /** @var JsonObject|null $pattern_properties */
        $pattern_properties = $this->getKeyword('patternProperties', ['object']);
        return $pattern_properties;
    }

    /**
     * @return JsonObject|null
     */
    public function getAdditionalProperties()
    {
        /** @var JsonObject|null $additional_properties */
        $additional_properties = $this->getKeyword('additionalProperties', ['object']);
        return $additional_properties;
    }

    /**
     * @return JsonObject|null
     */
    public function getDependencies()
    {
        /** @var JsonObject|null $dependencies */
        $dependencies = $this->getKeyword('dependencies', ['object']);
        return $dependencies;
    }
}
