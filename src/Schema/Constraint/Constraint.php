<?php
namespace d84\Phson\Schema\Constraint;

use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Schema\Exception\Constraint\ConstraintException;
use d84\Phson\Schema\SchemaAbstract;
use d84\Phson\Schema\RefSchema;

/**
 * The basic class that delegates the check to the corresponding constraint validator.
 */
abstract class Constraint
{
    /**
     * @param  JsonElementAbstract  $element
     * @param  JsonObject           $schema
     * @param  SchemaAbstract|null  $parent
     * @return void
     *
     * @throws ConstraintException
     */
    public static function validate(JsonElementAbstract $element, JsonObject $schema, SchemaAbstract $parent = null)
    {
        if ($schema->has('$ref')) {
            /** @var RefSchema $ref */
            $ref = SchemaAbstract::createSchema('$ref', $schema, $parent);
            $schema = $ref->resolve();
        }

        $schema_types = SchemaAbstract::getSchemaType($schema);
        $element_type = $element->getType();

        if ($element_type === 'number') {
            /** @var JsonPrimitive $primitive */
            $primitive = $element;
            if ($primitive->isInteger()) {
                $element_type = 'integer';
            }
            if ($primitive->isFloat()) {
                $element_type = 'float';
            }
        }

        if (in_array('number', $schema_types)) {
            $schema_types[] = 'integer';
            $schema_types[] = 'float';
        }

        if (!in_array($element_type, $schema_types)) {
            $expected_type = implode(',', $schema_types);
            $msg = "Element type mismatch: expected '$expected_type', but supplied '$element_type'";
            throw new ConstraintException($msg);
        }

        // These types are validated only by their types
        if ($element_type === 'null' || $element_type === 'boolean') {
            return;
        }

        // Look for constraint class
        $constraint_name  = self::getConstraintName($element_type);
        $constraint       = 'd84\Phson\Schema\Constraint\\' . ucfirst($constraint_name) . 'Constraint';
        if (!class_exists($constraint)) {
            $msg = "Not found constraint for the '$constraint_name' schema, class='$constraint'";
            throw new ConstraintException($msg);
        }

        // Convert JsonObject -> Schema (StringSchema, NumberSchema,...)
        $schema = SchemaAbstract::createSchema($element_type, $schema, $parent);

        if (!is_null($schema)) {
            // Validate!
            $constraint::validate($element, $schema);
        }
    }

    /**
     * @param  string $element_type
     * @return string
     */
    public static function getConstraintName($element_type)
    {
        if ($element_type === 'integer' || $element_type === 'float') {
            return 'number';
        }
        return $element_type;
    }
}
