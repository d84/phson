<?php
namespace d84\Phson\Schema\Constraint;

use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Schema\NumberSchema;

use d84\Phson\Schema\Exception\Constraint\Number\NumberConstraintException;
use d84\Phson\Schema\Exception\Constraint\Number\ValueOutOfRange;
use d84\Phson\Schema\Exception\Constraint\Number\ValueNotMultipleOf;

/**
 * NumberConstraint
 *
 * {"type": "number"}
 * {"type": "integer"}
 * {"type": "float"}
 *
 * Supported restrictions:
 * + minimum
 * + maximum
 * + exclusiveMaximum
 * + exclusiveMinimum
 * + multipleOf
 */
abstract class NumberConstraint
{
    /**
     * @param  JsonPrimitive  $element
     * @param  NumberSchema   $schema
     * @return void
     */
    public static function validate(JsonPrimitive $element, NumberSchema $schema)
    {
        self::checkMaximum($element, $schema);
        self::checkMinimum($element, $schema);
        self::multipleOf($element, $schema);
    }

    /**
     * @param  JsonPrimitive $element
     * @param  NumberSchema  $schema
     * @return void
     *
     * @throws ValueOutOfRange
     */
    public static function checkMaximum(JsonPrimitive $element, NumberSchema $schema)
    {
        $maximum           = $schema->getMaximum();
        $exclusive_maximum = $schema->getExclusiveMaximum();

        if (is_null($maximum)) {
            return;
        }

        $value = $element->getRaw();

        if ($exclusive_maximum === true && $value > $maximum) {
            throw new ValueOutOfRange($value, '>', $maximum);
        }
        if ($exclusive_maximum === false && $value >= $maximum) {
            throw new ValueOutOfRange($value, '>=', $maximum);
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @param  NumberSchema  $schema
     * @return void
     *
     * @throws ValueOutOfRange
     */
    public static function checkMinimum(JsonPrimitive $element, NumberSchema $schema)
    {
        $minimum           = $schema->getMinimum();
        $exclusive_minimum = $schema->getExclusiveMinimum();

        if (is_null($minimum)) {
            return;
        }

        $value = $element->getRaw();

        if ($exclusive_minimum === true && $value < $minimum) {
            throw new ValueOutOfRange($value, '<', $minimum);
        }
        if ($exclusive_minimum === false && $value <= $minimum) {
            throw new ValueOutOfRange($value, '<=', $minimum);
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @param  NumberSchema  $schema
     * @return void
     *
     * @throws ValueNotMultipleOf
     */
    public static function multipleOf(JsonPrimitive $element, NumberSchema $schema)
    {
        $mulof = $schema->getMultipleOf();

        if (is_null($mulof)) {
            return;
        }

        $value = $element->getRaw();

        if ($value % $mulof !== 0) {
            throw new ValueNotMultipleOf($value, $mulof);
        }
    }
}
