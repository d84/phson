<?php
namespace d84\Phson\Schema\Constraint;

use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Schema\StringSchema;

use d84\Phson\Schema\Exception\Constraint\String\StringConstraintException;
use d84\Phson\Schema\Exception\Constraint\String\ValueDoesNotMatchPattern;
use d84\Phson\Schema\Exception\Constraint\String\ValueDoesNotMatchFormat;
use d84\Phson\Schema\Exception\Constraint\String\ValueLengthMoreThanMax;
use d84\Phson\Schema\Exception\Constraint\String\ValueLengthLessThanMin;
use d84\Phson\Schema\Exception\Constraint\Enum\ValueOutOfScope;

/**
 * StringConstraint
 *
 * {"type": "string"}
 *
 * Supported restrictions:
 * + minLength
 * + maxLength
 * + pattern
 * + format: [date-time, date, email, ip-address, ipv6, ip, uri, mac]
 *
 * For the 'date-time' format supports additional attribute - 'dt-format', which represents format for date-time string.
 * By default that attribute is "Y-m-d\TH:i:sZ".
 */
abstract class StringConstraint
{
    /**
     * @param  JsonPrimitive  $element
     * @param  StringSchema   $schema
     * @return void
     */
    public static function validate(JsonPrimitive $element, StringSchema $schema)
    {
        self::checkLength($element, $schema);
        self::checkPattern($element, $schema);
        self::checkFormat($element, $schema);
        self::checkEnum($element, $schema);
        self::checkConst($element, $schema);
    }

    /**
     * @param  JsonPrimitive  $element
     * @param  StringSchema   $schema
     * @return void
     *
     * @throws ValueLengthMoreThanMax
     * @throws ValueLengthLessThanMin
     */
    public static function checkLength(JsonPrimitive $element, StringSchema $schema)
    {
        $maxlen = $schema->getMaxLength();
        $minlen = $schema->getMinLength();

        if (is_null($maxlen) && is_null($minlen)) {
            return;
        }

        $len = strlen($element->getRaw());

        if (!is_null($maxlen) && $len > $maxlen) {
            throw new ValueLengthMoreThanMax($element->getRaw(), $len, $maxlen);
        }
        if (!is_null($minlen) && $len < $minlen) {
            throw new ValueLengthLessThanMin($element->getRaw(), $len, $minlen);
        }
    }

    /**
     * @param  JsonPrimitive  $element
     * @param  StringSchema   $schema
     * @return void
     *
     * @throws ValueDoesNotMatchPattern
     */
    public static function checkPattern(JsonPrimitive $element, StringSchema $schema)
    {
        $pattern = $schema->getPattern();

        if (is_null($pattern)) {
            return;
        }

        $value = $element->getRaw();
        if (!preg_match($pattern, $value)) {
            throw new ValueDoesNotMatchPattern($value, $pattern);
        }
    }

    /**
     * @param  JsonPrimitive   $element
     * @param  StringSchema    $schema
     * @return void
     *
     * @throws ValueOutOfScope
     */
    public static function checkEnum(JsonPrimitive $element, StringSchema $schema)
    {
        $enum = $schema->getEnum();

        if (empty($enum)) {
            return;
        }

        if (!in_array($element->getRaw(), $enum)) {
            throw new ValueOutOfScope($element->getRaw(), $enum);
        }
    }

    /**
     * @param  JsonPrimitive  $element
     * @param  StringSchema   $schema
     * @return void
     *
     * @throws StringConstraintException
     */
    public static function checkConst(JsonPrimitive $element, StringSchema $schema)
    {
        $const = $schema->getConst();

        if (is_null($const)) {
            return;
        }

        $value = $element->getRaw();
        if ($value !== $const) {
            throw new StringConstraintException("Const validation failed: $value != $const");
        }
    }

    /**
     * @param  JsonPrimitive  $element
     * @param  StringSchema   $schema
     * @return void
     *
     * @throws StringConstraintException
     */
    public static function checkFormat(JsonPrimitive $element, StringSchema $schema)
    {
        $format = $schema->getFormat();

        if (is_null($format)) {
            return;
        }

        switch ($format) {
            case 'date-time':
                $dtformat = $schema->getNVL('dt-format', ['string'], 'Y-m-d\TH:i:sZ');
                self::checkFormatDateTime($element, $dtformat);
                return;
            case 'date':
                self::checkFormatDate($element);
                return;
            case 'email':
                self::checkFormatEmail($element);
                return;
            case 'ip-address':
                self::checkFormatIPAddress($element);
                return;
            case 'ipv6':
                self::checkFormatIPv6($element);
                return;
            case 'ip':
                self::checkFormatIP($element);
                return;
            case 'uri':
                self::checkFormatUri($element);
                return;
            case 'mac':
                self::checkFormatMac($element);
                return;
        }

        throw new StringConstraintException("Unknown format: $format");
    }

    /**
     * @param  JsonPrimitive  $element
     * @param  string         $dtformat
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatDateTime(JsonPrimitive $element, $dtformat = 'Y-m-d\TH:i:sZ')
    {
        if (!$element->isDate($dtformat)) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), "date-time[$dtformat]");
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatDate(JsonPrimitive $element)
    {
        if (!$element->isDate("Y-m-d")) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), 'date[Y-m-d]');
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatEmail(JsonPrimitive $element)
    {
        if (filter_var($element->getRaw(), FILTER_VALIDATE_EMAIL) === false) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), 'email');
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatIPAddress(JsonPrimitive $element)
    {
        if (filter_var($element->getRaw(), FILTER_VALIDATE_IP | FILTER_FLAG_IPV4) === false) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), 'ip-address');
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatIPv6(JsonPrimitive $element)
    {
        if (filter_var($element->getRaw(), FILTER_VALIDATE_IP | FILTER_FLAG_IPV6) === false) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), 'ipv6');
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatUri(JsonPrimitive $element)
    {
        if (filter_var($element->getRaw(), FILTER_VALIDATE_URL) === false) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), 'url');
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatMac(JsonPrimitive $element)
    {
        if (filter_var($element->getRaw(), FILTER_VALIDATE_MAC) === false) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), 'mac');
        }
    }

    /**
     * @param  JsonPrimitive $element
     * @return void
     *
     * @throws ValueDoesNotMatchFormat
     */
    public static function checkFormatIP(JsonPrimitive $element)
    {
        if (filter_var($element->getRaw(), FILTER_VALIDATE_IP) === false) {
            throw new ValueDoesNotMatchFormat($element->getRaw(), 'ip');
        }
    }
}
