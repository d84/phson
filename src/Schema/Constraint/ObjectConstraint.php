<?php
namespace d84\Phson\Schema\Constraint;

use Exception;
use RuntimeException;

use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Schema\Constraint\Constraint;
use d84\Phson\Schema\ObjectSchema;
use d84\Phson\Schema\SchemaAbstract;
use d84\Phson\Schema\Exception\Constraint\Object\ObjectConstraintException;
use d84\Phson\Schema\Exception\Constraint\Object\NotFoundRequiredProperty;

/**
 * ObjectConstraint
 *
 * {"type": "object"}
 *
 * Supported restrictions:
 * + required
 * + properties
 * + additionalProperties
 * + patternProperties
 * + minProperties
 * + maxProperties
 * + dependencies
 */
abstract class ObjectConstraint
{
    /**
     * @param  JsonObject   $element
     * @param  ObjectSchema $schema
     * @return void
     */
    public static function validate(JsonObject $element, ObjectSchema $schema)
    {
        self::checkSize($element, $schema);
        self::check($element, $schema);
    }

    /**
     * @param  JsonObject   $element
     * @param  ObjectSchema $schema
     * @return void
     *
     * @throws ObjectConstraintException
     */
    public static function checkSize(JsonObject $element, ObjectSchema $schema)
    {
        $minprops = $schema->getMinProperties();
        $maxprops = $schema->getMaxProperties();

        if (is_null($minprops) && is_null($maxprops)) {
            return;
        }

        $count = $element->getPropertiesCount();

        if (!is_null($maxprops) && $count > $maxprops) {
            throw new ObjectConstraintException("Input object properties count '$count' more than max '$maxprops'");
        }
        if (!is_null($minprops) && $count < $minprops) {
            throw new ObjectConstraintException("Input object properties count '$count' less than min '$minprops'");
        }
    }

    /**
     * @param  JsonObject   $element
     * @param  ObjectSchema $schema
     * @return void
     *
     * @throws NotFoundRequiredProperty
     */
    public static function check(JsonObject $element, ObjectSchema $schema)
    {
        // Keyword: properties
        $checked_props = self::checkProperties($element, $schema);

        // Keyword: patternProperties
        $checked_props = self::checkPatternProperties($element, $schema, $checked_props);

        // Keyword: additionalProperties
        $checked_props = self::checkAdditionalProperties($element, $schema, $checked_props);

        // Keyword: required
        $required = array_diff($schema->getRequired(), $checked_props);
        if (!empty($required)) {
            throw new NotFoundRequiredProperty($required);
        }

        self::checkDependencies($element, $schema);
    }

    /**
     * @param  JsonObject   $element
     * @param  ObjectSchema $schema
     * @return array
     */
    public static function checkProperties(JsonObject $element, ObjectSchema $schema)
    {
        $properties = $schema->getProperties();

        if (is_null($properties)) {
            return [];
        }

        $checked_props       = [];
        $properties_iterator = $properties->getIterator();

        foreach ($properties_iterator as $property_name => $property_schema) {
            if (isset($element[$property_name])) {
                $property         = $element[$property_name];
                /** @var JsonObject $property_schema */
                $property_schema  = JsonElementAbstract::wrap($property_schema);
                self::validateProperty("properties/$property_name", $property, $property_schema, $schema);
                $checked_props[] = $property_name;
            }
        }

        return $checked_props;
    }

    /**
     * @param  JsonObject   $element
     * @param  ObjectSchema $schema
     * @param  array        $checked_props
     * @return array
     */
    public static function checkPatternProperties(JsonObject $element, ObjectSchema $schema, array $checked_props)
    {
        $pattern_properties = $schema->getPatternProperties();

        if (is_null($pattern_properties)) {
            return $checked_props;
        }

        $unchecked_props = array_diff($element->getPropertyKeys(), $checked_props);

        foreach ($unchecked_props as $property_name) {
            $pattern_properties_it = $pattern_properties->getIterator();
            $pass = false;
            foreach ($pattern_properties_it as $pattern => $property_schema) {
                if (preg_match($pattern, $property_name)) {
                    $property         = $element[$property_name];
                    /** @var JsonObject $property_schema */
                    $property_schema  = JsonElementAbstract::wrap($property_schema);
                    self::validateProperty("patternProperties/$property_name", $property, $property_schema, $schema);
                    $pass = true;
                    break;
                }
            }
            if ($pass) {
                $checked_props[] = $property_name;
            }
        }

        return $checked_props;
    }

    /**
     * @param  JsonObject   $element
     * @param  ObjectSchema $schema
     * @param  array        $checked_props
     * @return array
     */
    public static function checkAdditionalProperties(JsonObject $element, ObjectSchema $schema, array $checked_props)
    {
        // additionalProperties
        $additional_properties = $schema->getAdditionalProperties();

        if (is_null($additional_properties)) {
            return $checked_props;
        }

        /** @var JsonObject $additional_properties */
        $additional_properties = JsonElementAbstract::wrap($additional_properties);

        // Get array of an unchecked properties
        $unchecked_props = array_diff($element->getPropertyKeys(), $checked_props);

        foreach ($unchecked_props as $property_name) {
            $property = $element[$property_name];
            self::validateProperty("additionalProperties/$property_name", $property, $additional_properties, $schema);
            $checked_props[] = $property_name;
        }

        return $checked_props;
    }

    /**
     * This keyword specifies rules that are evaluated if the instance is an object and contains a certain property.
     *
     * @param  JsonObject   $element
     * @param  ObjectSchema $schema
     * @return void
     *
     * @throws NotFoundRequiredProperty
     */
    public static function checkDependencies(JsonObject $element, ObjectSchema $schema)
    {
        $dependencies = $schema->getDependencies();

        if (is_null($dependencies)) {
            return;
        }

        $iterator = $dependencies->getIterator();

        foreach ($iterator as $property => $dependency) {
            if (isset($element[$property])) {
                if (is_array($dependency)) {
                    $unknown = $element->checkProperties($dependency);
                    if (!empty($unknown)) {
                        throw new NotFoundRequiredProperty($unknown);
                    }
                } elseif (is_object($dependency)) {
                    /** @var JsonObject $dependency_schema */
                    $dependency_schema = JsonElementAbstract::wrap($dependency);
                    Constraint::validate($element[$property], $dependency_schema, $schema);
                } else {
                    throw new ObjectConstraintException("Each dependency value MUST be an " .
                                                        "array or a valid JSON Schema");
                }
            }
        }
    }

    /**
     * Checks element against the supplied schema
     *
     * @param  string              $path
     * @param  JsonElementAbstract $e
     * @param  JsonObject          $schema
     * @param  SchemaAbstract      $parent
     * @return void
     *
     * @throws ObjectConstraintException
     */
    private static function validateProperty($path, JsonElementAbstract $e, JsonObject $schema, SchemaAbstract $parent)
    {
        try {
            Constraint::validate($e, $schema, $parent);
        } catch (RuntimeException $rte) {
            throw new ObjectConstraintException("Object property '$path' validation failed: " . $rte->getMessage());
        } catch (Exception $ce) {
            throw new ObjectConstraintException("Object property '$path' validation failed: " . $ce->getMessage());
        }
    }
}
