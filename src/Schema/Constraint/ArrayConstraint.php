<?php
namespace d84\Phson\Schema\Constraint;

use Exception;
use RuntimeException;

use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Document\Element\JsonArray;
use d84\Phson\Document\Element\JsonObject;

use d84\Phson\Schema\ArraySchema;
use d84\Phson\Schema\SchemaAbstract;

use d84\Phson\Schema\Constraint\Constraint;

use d84\Phson\Schema\Exception\Constraint\TArray\ArrayConstraintException;
use d84\Phson\Schema\Exception\Constraint\TArray\ArrayLengthMoreThanMax;
use d84\Phson\Schema\Exception\Constraint\TArray\ArrayLengthLessThanMin;
use d84\Phson\Schema\Exception\Constraint\TArray\ArrayNotUnique;

/**
 * ArrayConstraint
 *
 * {"type": "array"}
 *
 * Supported restrictions:
 * + items
 * + minItems
 * + maxItems
 * + additionalItems
 * + uniqueItems
 */
abstract class ArrayConstraint
{
    /**
     * @param  JsonArray    $element
     * @param  ArraySchema  $schema
     * @return void
     */
    public static function validate(JsonArray $element, ArraySchema $schema)
    {
        self::checkSize($element, $schema);
        self::checkUniqueItems($element, $schema);
        if (self::checkContains($element, $schema) === true) {
            return;
        }
        self::checkItems($element, $schema);
    }

    /**
     * @param  JsonArray   $element
     * @param  ArraySchema $schema
     * @return void
     */
    public static function checkItems(JsonArray $element, ArraySchema $schema)
    {
        $items = $schema->getItems();

        if (is_null($items)) {
            return;
        }
        if ($items instanceof JsonArray) {
            self::checkArray($element, $items, $schema);
        }
        if ($items instanceof JsonObject) {
            self::checkObject($element, $items, $schema);
        }
    }

    /**
     * @param  JsonArray   $element
     * @param  ArraySchema $schema
     * @return void
     *
     * @throws ArrayLengthMoreThanMax
     * @throws ArrayLengthLessThanMin
     */
    public static function checkSize(JsonArray $element, ArraySchema $schema)
    {
        $min_items = $schema->getMinItems();
        $max_items = $schema->getMaxItems();

        $size = $element->size();

        if (!is_null($min_items) && $size < $min_items) {
            throw new ArrayLengthLessThanMin($size, $min_items);
        }
        if (!is_null($max_items) && $size > $max_items) {
            throw new ArrayLengthMoreThanMax($size, $max_items);
        }
    }

    /**
     * @param  JsonArray   $element
     * @param  JsonArray   $items
     * @param  ArraySchema $schema
     * @return void
     *
     * @throws ArrayConstraintException
     */
    public static function checkArray(JsonArray $element, JsonArray $items, ArraySchema $schema)
    {
        $array_size       = $element->size();
        $item_size        = $items->size();
        $additional_items = $schema->getAdditionalItems();

        if ($array_size > $item_size && is_null($additional_items)) {
            throw new ArrayConstraintException("Not defined 'additionalItems' keywords");
        }

        for ($i = 0; $i < $item_size; $i++) {
            self::validateItem($i, $element[$i], $items[$i], $schema);
        }

        for ($i = $item_size; $i < $array_size; $i++) {
            self::validateItem($i, $element[$i], $additional_items, $schema);
        }
    }

    /**
     * @param  JsonArray   $element
     * @param  JsonObject  $items
     * @param  ArraySchema $schema
     * @return void
     */
    public static function checkObject(JsonArray $element, JsonObject $items, ArraySchema $schema)
    {
        for ($i = 0; $i < $element->size(); $i++) {
            self::validateItem($i, $element[$i], $items, $schema);
        }
    }

    /**
     * @param  JsonArray   $element
     * @param  ArraySchema $schema
     * @return void
     *
     * @throws ArrayNotUnique
     */
    public static function checkUniqueItems(JsonArray $element, ArraySchema $schema)
    {
        if (!$schema->isUniqueItems()) {
            return;
        }
        $unique = array_unique($element->getAll());
        if ($element->size() != count($unique)) {
            throw new ArrayNotUnique();
        }
    }

    /**
     * @param  JsonArray   $element
     * @param  ArraySchema $schema
     * @return bool
     */
    public static function checkContains(JsonArray $element, ArraySchema $schema)
    {
        /** @var JsonObject|null $contains_schema */
        $contains_schema = $schema->getContains();

        if (is_null($contains_schema)) {
            return false;
        }

        /*
          An array instance is valid against "contains" if at least one of
           itselements is valid against the given schema.
        */
        for ($i = 0; $i < $element->size(); $i++) {
            if ($i > 0) {
                return true;
            }
            self::validateItem($i, $element[$i], $contains_schema, $schema);
        }
    }

    /**
     * Checks element against the supplied schema
     *
     * @param  int                 $index
     * @param  JsonElementAbstract $el
     * @param  JsonObject          $schema
     * @param  SchemaAbstract      $parent
     * @return void
     *
     * @throws ArrayConstraintException
     */
    private static function validateItem($index, JsonElementAbstract $el, JsonObject $schema, SchemaAbstract $parent)
    {
        try {
            Constraint::validate($el, $schema, $parent);
        } catch (RuntimeException $rte) {
            throw new ArrayConstraintException("Array element '$index' validation failed: " . $rte->getMessage());
        } catch (Exception $ce) {
            throw new ArrayConstraintException("Array element '$index' validation failed: " . $ce->getMessage());
        }
    }
}
