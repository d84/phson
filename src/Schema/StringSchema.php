<?php
namespace d84\Phson\Schema;

use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Schema\Exception\JsonSchemaException;

class StringSchema extends SchemaAbstract
{
    public function __construct(JsonObject $schema, SchemaAbstract $parent = null)
    {
        parent::__construct($schema, $parent);
    }

    /**
     * @return int|null
     *
     * @throws JsonSchemaException
     */
    public function getMaxLength()
    {
        /** @var JsonPrimitive|null $maxLength */
        $maxLength = $this->getKeyword('maxLength', ['number']);

        if (is_null($maxLength)) {
            return null;
        }

        $maxLength = $maxLength->asInteger();
        if ($maxLength < 0) {
            $msg = "Keyword 'maxLength' must be a non-negative integer, but supplied '$maxLength'";
            throw new JsonSchemaException($msg);
        }

        return $maxLength;
    }

    /**
     * @return int|null
     *
     * @throws JsonSchemaException
     */
    public function getMinLength()
    {
        /** @var JsonPrimitive|null $minLength */
        $minLength = $this->getKeyword('minLength', ['number']);

        if (is_null($minLength)) {
            return null;
        }

        $minLength = $minLength->asInteger();
        if ($minLength < 0) {
            $msg = "Keyword 'minLength' must be a non-negative integer, but supplied '$minLength'";
            throw new JsonSchemaException($msg);
        }

        return $minLength;
    }

    /**
     * @return string|null
     *
     * @throws JsonSchemaException
     */
    public function getFormat()
    {
        /** @var JsonPrimitive|null $format */
        $format = $this->getKeyword('format', ['string']);

        if (is_null($format)) {
            return null;
        }

        $format = $format->asString();
        if (empty($format)) {
            throw new JsonSchemaException('Keyword "format" is empty');
        }

        return $format;
    }

    /**
     * @return string|null
     *
     * @throws JsonSchemaException
     */
    public function getPattern()
    {
        /** @var JsonPrimitive|null $pattern */
        $pattern = $this->getKeyword('pattern', ['string']);

        if (is_null($pattern)) {
            return null;
        }

        $pattern = $pattern->asString();
        if (empty($pattern)) {
            throw new JsonSchemaException('Keyword "pattern" is empty. This string SHOULD be a ' .
                                          'valid regular expression, according to the ECMA 262 ' .
                                          'regular expression dialect');
        }

        return $pattern;
    }
}
