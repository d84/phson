<?php
namespace d84\Phson\Schema;

use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonArray;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Schema\Exception\JsonSchemaException;

define(
    'PHSON_JSON_SCHEMA_TYPES',
    [
      'null',
      'boolean',
      'object',
      'array',
      'number',
      'integer',
      'float',
      'string',
      '$ref'
    ]
);

/**
 * SchemaAbstract
 */
abstract class SchemaAbstract
{
    /** @var array */
    private $type;
    /** @var SchemaAbstract */
    private $parent;
    /** @var JsonObject */
    protected $schema;

    /**
     * @param JsonObject           $schema
     * @param SchemaAbstract|null  $parent
     */
    public function __construct(JsonObject $schema, SchemaAbstract $parent = null)
    {
        $this->schema = $schema;
        $this->parent = $parent;
        $this->type   = self::getSchemaType($this->schema);
    }

    /**
     * @return SchemaAbstract|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return JsonObject
     */
    public function getJsonObject()
    {
        return $this->schema;
    }

    /**
     * @return SchemaAbstract|null
     */
    public function getRoot()
    {
        $root = $this->getParent();
        if (is_null($root)) {
            return null;
        }
        for (;;) {
            if (is_null($root->getParent())) {
                return $root;
            }
            $root = $root->getParent();
        }
        return null;
    }

    /**
     * Returns array of supported types.
     * String values MUST be one of the six primitive types ("null", "boolean", "object", "array",
     * "number", or "string"), or "integer"  which matches any number with a zero fractional part.
     *
     * @return array
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSchema()
    {
        return $this->getNVL('$schema', ['string'], '');
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->getNVL('$ref', ['string'], '');
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->getNVL('id', ['string'], '');
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getNVL('title', ['string'], '');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getNVL('description', ['string'], '');
    }

    /**
     * @return ObjectSchema|null
     */
    public function getDefinitions()
    {
        /** @var JsonObject|null $definitions */
        $definitions = $this->getKeyword('definitions', ['object']);
        if (is_null($definitions)) {
            return null;
        }
        return new ObjectSchema($definitions);
    }

    /**
     * @return array|null
     *
     * @throws JsonSchemaException
     */
    public function getEnum()
    {
        $keyword = $this->getKeyword('enum', ['array']);

        if (is_null($keyword)) {
            return null;
        }

        $enum = $keyword->getRaw(); // Array
        if (empty($enum)) {
            throw new JsonSchemaException("Enum SHOULD have at least one element");
        }

        return $enum;
    }

    /**
     * @return mixed
     */
    public function getConst()
    {
        $keyword = $this->getKeyword('const', ["null",  "boolean", "object", "array", "number", "string"]);

        if (is_null($keyword)) {
            return null;
        }

        return $keyword->getRaw();
    }

    /**
     * @param  string  $keyword
     * @param  array   $allowed_types
     * @param  bool    $is_mandatory
     * @return JsonElementAbstract|null
     *
     * @throws JsonSchemaException
     */
    public function getKeyword($keyword, array $allowed_types, $is_mandatory = false)
    {
        /** @var JsonElementAbstract|null $element */
        $element = $this->schema->get($keyword);

        if (is_null($element)) {
            if ($is_mandatory) {
                throw new JsonSchemaException("Not found mandatory keyword '$keyword'");
            }
            return null;
        }

        $type = $element->getType();
        if (!in_array($type, $allowed_types)) {
            if (count($allowed_types) > 1) {
                $allowed_types = 'one of [' . implode(',', $allowed_types) . ']';
            } else {
                $allowed_types = $allowed_types[0];
            }
            throw new JsonSchemaException("The value of '$keyword' keyword MUST be $allowed_types, but supplied $type");
        }

        return $element;
    }

    /**
     * @param  string $keyword
     * @param  array  $allowed_types
     * @param  mixed  $default
     * @return mixed
     */
    public function getNVL($keyword, array $allowed_types, $default)
    {
        $element = $this->getKeyword($keyword, $allowed_types);
        if (is_null($element)) {
            return $default;
        }
        return $element->getRaw();
    }

    /**
     * @param  JsonObject $schema
     * @return array
     *
     * @throws JsonSchemaException
     */
    public static function getSchemaType(JsonObject $schema)
    {
        /** @var JsonElementAbstract|null $keyword */
        $keyword = $schema->get('type');

        if (is_null($keyword)) {
            throw new JsonSchemaException("Not found mandatory keyword 'type' in schema: " . (string)$schema);
        }

        $schema_type = null;
        switch ($keyword->getType()) {
            case 'array':
                /** @var JsonArray $array */
                $array = $keyword;
                $schema_type = $array->getAll(false);
                break;
            case 'string':
                /** @var JsonPrimitive $str */
                $str = $keyword;
                $schema_type = [$str->asString()];
                break;
            default:
                throw new JsonSchemaException("The value of 'type' keyword MUST be an" .
                                          " array or string, but supplied '$schema_type' in schema: ".(string)$schema);
        }

        $unkown_types = array_diff($schema_type, PHSON_JSON_SCHEMA_TYPES);
        if (!empty($unkown_types)) {
            $unkown_types = implode(',', $unkown_types);
            $known_types  = implode(',', PHSON_JSON_SCHEMA_TYPES);
            $msg = "The value MUST be one of the six primitive types ($known_types),".
                  " but supplied ($unkown_types) in schema:" . (string)$schema;
            throw new JsonSchemaException($msg);
        }

        array_walk($schema_type, function (&$item) {
            $item = strtolower($item);
        });

        return $schema_type;
    }

    /**
     * @param  string               $type
     * @param  JsonObject           $schema
     * @param  SchemaAbstract|null  $parent
     * @return SchemaAbstract|null
     *
     * @throws JsonSchemaException
     */
    public static function createSchema($type, JsonObject $schema, SchemaAbstract $parent = null)
    {
        switch ($type) {
            case '$ref':
                if (!$schema->has('type')) {
                    $schema->setProperty('type', '$ref');
                }
                return new RefSchema($schema, $parent);
            case 'string':
                return new StringSchema($schema, $parent);

            case 'integer':
            case 'number':
            case 'float':
                return new NumberSchema($schema, $parent);

            case 'null':
            case 'boolean':
                return null;

            case 'array':
                return new ArraySchema($schema, $parent);

            case 'object':
                return new ObjectSchema($schema, $parent);
        }
        throw new JsonSchemaException("Unsupported schema type '$type' in schema: " . (string)$schema);
    }
}
