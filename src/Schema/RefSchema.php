<?php
namespace d84\Phson\Schema;

use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Schema\Exception\JsonSchemaException;

/**
 * Schema Re-Use With "definitions"
 * The "definitions" keywords provides a standardized location for
 * schema authors to inline re-usable JSON Schemas into a more general
 * schema.  The keyword does not directly affect the validation result.
 *
 * This keyword's value MUST be an object.  Each member value of this
 * object MUST be a valid JSON Schema.
 *
 *   As an example, here is a schema describing an array of positive
 *   integers, where the positive integer constraint is a subschema in
 *   "definitions":
 *
 *   {
 *       "type": "array",
 *       "items": { "$ref": "#/definitions/positiveInteger" },
 *       "definitions": {
 *           "positiveInteger": {
 *               "type": "integer",
 *               "exclusiveMinimum": 0
 *           }
 *       }
 *   }
 */
class RefSchema extends SchemaAbstract
{
    public function __construct(JsonObject $schema, SchemaAbstract $parent = null)
    {
        parent::__construct($schema, $parent);
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->getNVL('$ref', ['string'], '');
    }

    /**
     * @return JsonObject
     *
     * @throws JsonSchemaException
     */
    public function resolve()
    {
        $ref = $this->getRef();
        if (empty($ref)) {
            throw new JsonSchemaException("The value of 'ref' keyword MUST be a non empty string");
        }

        $schema      = null;
        /** @var SchemaAbstract|null $root_schema */
        $root_schema = $this->getRoot();

        if (is_null($root_schema)) {
            throw new JsonSchemaException("Unresolved JSONPointer: '$ref'");
        }

        // Local pointer
        if ($ref[0] === '#') {
            $schema = $root_schema->getJsonObject()->find(ltrim($ref, '#'));
        }

        if (!$schema instanceof JsonObject) {
            throw new JsonSchemaException("Unresolved JSONPointer: '$ref'");
        }

        return $schema;
    }
}
