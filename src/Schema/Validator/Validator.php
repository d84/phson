<?php
namespace d84\Phson\Schema\Validator;

use InvalidArgumentException;
use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\JsonDocument;
use d84\Phson\Schema\Exception\JsonSchemaException;
use d84\Phson\Schema\Constraint\Constraint;

/**
 * https://tools.ietf.org/html/draft-handrews-json-schema-validation-00#page-12
 */
class Validator implements ValidatorInterface
{
    /**
     * Validate document by given JSON schema
     * Returns JsonElementAbstract which represents $document argument.
     *
     * @param  string|object $document
     * @param  string|object $schema
     * @return JsonElementAbstract
     *
     * @throws InvalidArgumentException
     * @throws JsonSchemaException
     */
    public function validate($document, $schema)
    {
        $document = $this->toJsonDocument($document);
        $schema   = $this->toJsonDocument($schema);

        if (!$schema instanceof JsonObject) {
            $supplied = gettype($schema);
            throw new JsonSchemaException("Schema must be valid JSON object, but supplied '$supplied'");
        }

        Constraint::validate($document, $schema);

        return $document;
    }

    /**
     * @param  string|object $value
     * @return JsonElementAbstract
     *
     * @throws InvalidArgumentException
     */
    private function toJsonDocument($value)
    {
        if (is_string($value)) {
            return JsonDocument::fromJsonString($value);
        }
        if (is_object($value)) {
            if ($value instanceof JsonElementAbstract) {
                return $value;
            }
            return JsonDocument::element($value);
        }
        $supplied = gettype($value);
        throw new InvalidArgumentException("Expected 'string' or 'object', but supplied '$supplied'");
    }
}
