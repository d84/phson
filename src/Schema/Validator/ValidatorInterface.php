<?php
namespace d84\Phson\Schema\Validator;

use d84\Phson\Document\Element\JsonElementAbstract;
use d84\Phson\Schema\Exception\JsonSchemaException;

/**
 * https://spacetelescope.github.io/understanding-json-schema/index.html
 */
interface ValidatorInterface
{
    /**
     * Validate document by given JSON schema.
     * Returns JsonElementAbstract which represents $document argument.
     *
     * @param  string|object $document
     * @param  string|object $schema
     * @return JsonElementAbstract
     *
     * @throws JsonSchemaException
     */
    public function validate($document, $schema);
}
