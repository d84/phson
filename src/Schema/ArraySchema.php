<?php
namespace d84\Phson\Schema;

use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonArray;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Schema\Exception\JsonSchemaException;

/**
 * ArraySchema
 */
class ArraySchema extends SchemaAbstract
{
    public function __construct(JsonObject $schema, SchemaAbstract $parent = null)
    {
        parent::__construct($schema, $parent);
    }

    /**
     * @return int|null
     *
     * @throws JsonSchemaException
     */
    public function getMaxItems()
    {
        /** @var JsonPrimitive|null $max_items */
        $max_items = $this->getKeyword('maxItems', ['number']);

        if (is_null($max_items)) {
            return null;
        }

        $max_items = $max_items->asInteger();
        if ($max_items < 0) {
            $msg = "Keyword 'maxItems' must be a non-negative integer, but supplied '$max_items'";
            throw new JsonSchemaException($msg);
        }

        return $max_items;
    }

    /**
     * @return int|null
     *
     * @throws JsonSchemaException
     */
    public function getMinItems()
    {
        /** @var JsonPrimitive|null $min_items */
        $min_items = $this->getKeyword('minItems', ['number']);

        if (is_null($min_items)) {
            return null;
        }

        $min_items = $min_items->asInteger();
        if ($min_items < 0) {
            $msg = "Keyword 'minItems' must be a non-negative integer, but supplied '$min_items'";
            throw new JsonSchemaException($msg);
        }

        return $min_items;
    }

    /**
     * @return JsonArray|JsonObject|null
     */
    public function getItems()
    {
        return $this->getKeyword('items', ['array', 'object']);
    }

    /**
     * @return JsonObject|null
     */
    public function getAdditionalItems()
    {
        /** @var JsonObject|null $additional_items */
        $additional_items = $this->getKeyword('additionalItems', ['object']);
        return $additional_items;
    }

    /**
     * @return bool|null
     */
    public function isUniqueItems()
    {
        /** @var bool|null $unique_items */
        $unique_items = $this->getKeyword('uniqueItems', ['boolean'], false);
        return $unique_items;
    }

    /**
     * @return JsonObject|null
     */
    public function getContains()
    {
        /** @var JsonObject|null $contains */
        $contains = $this->getKeyword('contains', ['object']);
        return $contains;
    }
}
