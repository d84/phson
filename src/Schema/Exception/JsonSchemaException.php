<?php
namespace d84\Phson\Schema\Exception;

use d84\Phson\Exception\JsonException;

/**
 * JsonSchemaException
 */
class JsonSchemaException extends JsonException
{
    /**
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
