<?php
namespace d84\Phson\Schema\Exception\Constraint\Number;

use d84\Phson\Schema\Exception\Constraint\ConstraintException;

/**
 * NumberConstraintException
 */
class NumberConstraintException extends ConstraintException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
