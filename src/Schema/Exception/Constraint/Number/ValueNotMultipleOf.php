<?php
namespace d84\Phson\Schema\Exception\Constraint\Number;

/**
 * ValueNotMultipleOf
 */
class ValueNotMultipleOf extends NumberConstraintException
{
    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "The value '%s' is not a multiple of '%s'";

    /**
     * __construct
     *
     * @param int|float $value
     * @param int|float $mulof
     */
    public function __construct($value, $mulof)
    {
        $message = sprintf(self::MESSAGE, strval($value), strval($mulof));
        parent::__construct($message);
    }
}
