<?php
namespace d84\Phson\Schema\Exception\Constraint\Number;

/**
 * ValueOutOfRange
 */
class ValueOutOfRange extends NumberConstraintException
{
    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "The value '%s' is %s allowed '%s'";

    /**
     * __construct
     *
     * @param int|float $value
     * @param string    $operation Value of [>,<,>=,<=]
     * @param int|float $threshold
     */
    public function __construct($value, $operation, $threshold)
    {
        $message = sprintf(
            self::MESSAGE,
            strval($value),
            $this->operationToString($operation),
            strval($threshold)
        );
        parent::__construct($message);
    }

    /**
     * operationToString
     *
     * @param  string $operation Allowed values: [>,<,>=,<=]
     * @return string
     */
    private function operationToString($operation)
    {
        switch ($operation) {
            case ">":
                $operation = "more than";
                break;
            case "<":
                $operation = "less than";
                break;
            case ">=":
                $operation ="more or equal than";
                break;
            case "<=":
                $operation ="less or equal than";
                break;
        }
        return $operation;
    }
}
