<?php
namespace d84\Phson\Schema\Exception\Constraint\TArray;

/**
 * ArrayNotUnique
 */
class ArrayNotUnique extends ArrayConstraintException
{
    /**
     * __construct
     */
    public function __construct()
    {
        parent::__construct("Array contains not unique elements");
    }
}
