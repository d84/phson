<?php
namespace d84\Phson\Schema\Exception\Constraint\TArray;

/**
 * ArrayLengthMoreThanMax
 */
class ArrayLengthMoreThanMax extends ArrayConstraintException
{
    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "The max length of an array is '%d' but supplied array has '%d' length";

    /**
     * __construct
     *
     * @param int $length
     * @param int $max
     */
    public function __construct($length, $max)
    {
        $message = sprintf(self::MESSAGE, $max, $length);

        parent::__construct($message);
    }
}
