<?php
namespace d84\Phson\Schema\Exception\Constraint\TArray;

use d84\Phson\Schema\Exception\Constraint\ConstraintException;

/**
 * ArrayConstraintException
 */
class ArrayConstraintException extends ConstraintException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
