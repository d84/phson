<?php
namespace d84\Phson\Schema\Exception\Constraint\TArray;

/**
 * ArrayLengthLessThanMin
 */
class ArrayLengthLessThanMin extends ArrayConstraintException
{
    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "The min length of an array is '%d' but supplied array has '%d' length";

    /**
     * __construct
     *
     * @param int $length
     * @param int $min
     */
    public function __construct($length, $min)
    {
        $message = sprintf(self::MESSAGE, $min, $length);

        parent::__construct($message);
    }
}
