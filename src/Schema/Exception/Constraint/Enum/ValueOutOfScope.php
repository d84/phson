<?php
namespace d84\Phson\Schema\Exception\Constraint\Enum;

/**
 * ValueOutOfScope
 */
class ValueOutOfScope extends EnumConstraintException
{
    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "The value '%s' is out of scope %s";

    /**
     * __construct
     *
     * @param mixed $value
     * @param array $scope
     */
    public function __construct($value, array $scope)
    {
        $message = sprintf(self::MESSAGE, strval($value), implode(',', $scope));

        parent::__construct($message);
    }
}
