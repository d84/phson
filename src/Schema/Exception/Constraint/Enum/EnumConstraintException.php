<?php
namespace d84\Phson\Schema\Exception\Constraint\Enum;

use d84\Phson\Schema\Exception\Constraint\ConstraintException;

/**
 * EnumConstraintException
 */
class EnumConstraintException extends ConstraintException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
