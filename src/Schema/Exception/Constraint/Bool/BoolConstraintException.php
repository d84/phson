<?php
namespace d84\Phson\Schema\Exception\Constraint\Bool;

use d84\Phson\Schema\Exception\Constraint\ConstraintException;

/**
 * BoolConstraintException
 */
class BoolConstraintException extends ConstraintException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
