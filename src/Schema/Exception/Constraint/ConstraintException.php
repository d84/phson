<?php
namespace d84\Phson\Schema\Exception\Constraint;

use d84\Phson\Schema\Exception\JsonSchemaException;

/**
 * ConstraintException
 */
class ConstraintException extends JsonSchemaException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
