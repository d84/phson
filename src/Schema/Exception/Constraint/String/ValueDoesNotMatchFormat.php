<?php
namespace d84\Phson\Schema\Exception\Constraint\String;

/**
 * ValueDoesNotMatchFormat
 */
class ValueDoesNotMatchFormat extends StringConstraintException
{
    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "The value '%s' does not match to the '%s' format";

    /**
     * __construct
     *
     * @param string $value
     * @param string $format
     */
    public function __construct($value, $format)
    {
        $message = sprintf(self::MESSAGE, $value, $format);

        parent::__construct($message);
    }
}
