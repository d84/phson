<?php
namespace d84\Phson\Schema\Exception\Constraint\String;

use d84\Phson\Schema\Exception\Constraint\ConstraintException;

/**
 * StringConstraintException
 */
class StringConstraintException extends ConstraintException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
