<?php
namespace d84\Phson\Schema\Exception\Constraint\String;

/**
 * ValueLengthLessThanMin
 */
class ValueLengthLessThanMin extends StringConstraintException
{
    /**
     * MESSAGE
     * @var string
     */
    const MESSAGE = "The string value length is '%d', but min allowed '%d' (%s)";

    /**
     * @param string  $value
     * @param int     $length
     * @param int     $max
     */
    public function __construct($value, $length, $max)
    {
        $message = sprintf(self::MESSAGE, $length, $max, $value);

        parent::__construct($message);
    }
}
