<?php
namespace d84\Phson\Schema\Exception\Constraint\String;

/**
 * ValueDoesNotMatchPattern
 */
class ValueDoesNotMatchPattern extends StringConstraintException
{
    /**
     * MESSAGE
     *
     * @var string
     */
    const MESSAGE = "The value '%s' does not match to the '%s' pattern";

    /**
     * __construct
     *
     * @param string $value
     * @param string $pattern
     */
    public function __construct($value, $pattern)
    {
        $message = sprintf(self::MESSAGE, $value, $pattern);

        parent::__construct($message);
    }
}
