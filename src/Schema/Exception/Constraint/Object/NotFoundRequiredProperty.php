<?php
namespace d84\Phson\Schema\Exception\Constraint\Object;

/**
 * NotFoundRequiredProperty
 */
class NotFoundRequiredProperty extends ObjectConstraintException
{
    /**
     * __construct
     *
     * @param string|array $property
     */
    public function __construct($property)
    {
        $message = "";

        if (is_array($property) && count($property) == 1) {
            $property = $property[0];
        }

        if (is_array($property)) {
            $message = sprintf("Not found properties '%s' in the object", implode(',', $property));
        } else {
            $message = sprintf("Not found property '%s' in the object", $property);
        }
        parent::__construct($message);
    }
}
