<?php
namespace d84\Phson\Schema\Exception\Constraint\Object;

use d84\Phson\Schema\Exception\Constraint\ConstraintException;

/**
 * ObjectConstraintException
 */
class ObjectConstraintException extends ConstraintException
{
    /**
     * __construct
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
