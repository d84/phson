<?php
namespace d84\Phson\Schema;

use d84\Phson\Document\Element\JsonObject;
use d84\Phson\Document\Element\JsonPrimitive;
use d84\Phson\Schema\Exception\JsonSchemaException;

/**
 * NumberSchema
 */
class NumberSchema extends SchemaAbstract
{
    public function __construct(JsonObject $schema, SchemaAbstract $parent = null)
    {
        parent::__construct($schema, $parent);
    }

    /**
     * @return int|float|null
     */
    public function getMaximum()
    {
        /** @var JsonPrimitive|null $maximum */
        $maximum = $this->getKeyword('maximum', ['number']);

        if (is_null($maximum)) {
            return null;
        }

        return $maximum->asNumber();
    }

    /**
     * @return int|float|null
     */
    public function getMinimum()
    {
        /** @var JsonPrimitive|null $minimum */
        $minimum = $this->getKeyword('minimum', ['number']);

        if (is_null($minimum)) {
            return null;
        }

        return $minimum->asNumber();
    }

    /**
     * @return bool
     */
    public function getExclusiveMaximum()
    {
        return $this->getNVL('exclusiveMaximum', ['boolean'], false);
    }

    /**
     * @return bool
     */
    public function getExclusiveMinimum()
    {
        return $this->getNVL('exclusiveMinimum', ['boolean'], false);
    }

    /**
     * @return int|float|null
     */
    public function getMultipleOf()
    {
        /** @var JsonPrimitive|null $number */
        $number = $this->getKeyword('multipleOf', ['number']);

        if (is_null($number)) {
            return null;
        }

        $number = $number->asNumber();

        if ($number < 0) {
            throw new JsonSchemaException("The value of 'multipleOf' MUST be a number, strictly greater than 0");
        }

        return $number;
    }
}
